-- MySQL dump 10.16  Distrib 10.2.22-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: proig_bd
-- ------------------------------------------------------
-- Server version	10.2.22-MariaDB-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fotosproduto`
--

DROP TABLE IF EXISTS `fotosproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotosproduto` (
  `idFoto` int(11) NOT NULL AUTO_INCREMENT,
  `nomefoto` varchar(120) NOT NULL,
  `idproduto` int(11) NOT NULL,
  PRIMARY KEY (`idFoto`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotosproduto`
--

LOCK TABLES `fotosproduto` WRITE;
/*!40000 ALTER TABLE `fotosproduto` DISABLE KEYS */;
INSERT INTO `fotosproduto` (`idFoto`, `nomefoto`, `idproduto`) VALUES (1,'20181229-165918999537557-CAPA.png',2),(2,'20181229-1708521651146701-matrizes.png',2),(3,'20181229-1717081202234743-galoLogo.jpg',2),(4,'20181229-1717211117875260-galobranco.jpeg',2),(5,'20181229-1721561583005555-IMG-20180821-WA0037.jpg',3),(6,'20181229-173245643700481-astra baixa.png',4),(7,'20181229-1738591058361890-Screenshot_1.png',4),(8,'20181229-1750301401406476-Screenshot_1.png',6),(9,'20181229-1755171620276678-IMG-20181222-WA0000.jpg',7),(10,'20181229-175521281086733-IMG-20181222-WA0000.jpg',7),(11,'20181229-17552290594564-IMG-20181222-WA0000.jpg',7),(12,'20181230-1400041572911215-IMG-20180614-WA0108.jpg',8),(13,'20181230-1420111892249280-IMG-20180614-WA0108.jpg',9),(14,'20181230-1422261501798500-Screenshot_20181230-142121_Chrome.jpg',4),(15,'20181231-170825298606745-IMG-20181231-WA0123.jpg',11),(16,'20190110-0944331673204573-IMG-20190109-WA0198.jpg',12),(17,'20190112-0820422128124446-IMG-20190111-WA0026.jpg',1),(18,'20190120-1613432007813993-20190116_165731.jpg',1);
/*!40000 ALTER TABLE `fotosproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `cidade` varchar(120) NOT NULL,
  `estado` char(2) NOT NULL,
  `criatorio` varchar(120) NOT NULL,
  `apelido` varchar(120) NOT NULL,
  `dn` date NOT NULL,
  `whatsapp` varchar(24) NOT NULL,
  `senha` varchar(18) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`id`, `nome`, `email`, `cidade`, `estado`, `criatorio`, `apelido`, `dn`, `whatsapp`, `senha`, `ativo`) VALUES (1,'Werdeson barbosa da silva','22studioweb@gmail.com','PATROCINIO','MG','meu ig','hÃº','1985-01-22','3891097630','123',1),(4,'Leandro Arantes EmatnÃ©','leandroematne@hotmail.com','Socorro','SP','Gigante das AmÃ©ricas','Leandro','0000-00-00','19999121216','153624',1),(3,'JosÃ© Phillippe Palhares Spirandeli','josepps02@gmail.com','Uberaba','MG','IG PALHARES','ZÃ‰ PHELLIPPE','1999-10-02','34998950616','josegol99',1),(5,'Hudson Carolino ','carolinohudson@gmail.com','Curitiba','PR','Hudsoncarolino','Hudsoncarolino','0000-00-00','123','123',1),(6,'Thiago de Souza Neto Silva','tsnsta@outlook.com','ARACAJU','SE','+Q7\'','d\'SOUZA','0000-00-00','079 998060607','007007',0),(7,'central','CENTRALDOigtm2018@gmail.com','Uberaba','MG','Central do Ã­ndio gigante do triÃ¢ngulo mineiro','central','0000-00-00','34988527333','centraldoigtm2018#',0),(8,'central','CENTRALDOigtm@gmail.com','Uberaba','MG','Central do Ã­ndio gigante do triÃ¢ngulo mineiro','central','0000-00-00','34988527333','centraldoigtm2018#',1),(9,'Enzo gabriel colares e silva','midiaptc@gmail.com','Patrocinio - mg','MG','eg ig','enzo','0000-00-00','38991097630','padrao2018@',0),(10,'henrique capistrano','henriquepc96@hotmail.com','santa rita do sapucai','MG','indio gigante do anil','ig do anil','0000-00-00','35998949622','hpc10196',0),(11,'JosÃ© Phellippe','promarkd2018@gmail.com','Uberaba','MG','pRÃ“ MARKD','ZÃ‰','0000-00-00','34998950616','JOSEGOL99',1),(12,'Luan Rodrigues ','luanrodrigues1160@gmail.com','Guaraciaba sc ','SC','Criatorio lr','Lr','0000-00-00','988185379','123',1),(13,'MATHEUS HENRIQUE GOMES ROSA','matheushenrique_gomes@hotmail.com','PatrocÃ­nio','MG','Gr Ã­ndio gigante','Gr','0000-00-00','34 991759295','mhgr4422',1);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `idproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(120) NOT NULL,
  `medida` varchar(60) DEFAULT NULL,
  `tipo` varchar(60) NOT NULL,
  `crista` varchar(60) DEFAULT NULL,
  `idade` varchar(60) DEFAULT NULL,
  `preco` varchar(60) NOT NULL,
  `asas` varchar(60) DEFAULT NULL,
  `barbela` varchar(60) DEFAULT NULL,
  `rabo` varchar(60) DEFAULT NULL,
  `retirada` varchar(60) NOT NULL,
  `descricao` text NOT NULL,
  `id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp(),
  `qtd` varchar(20) NOT NULL,
  `und` varchar(5) NOT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` (`idproduto`, `titulo`, `medida`, `tipo`, `crista`, `idade`, `preco`, `asas`, `barbela`, `rabo`, `retirada`, `descricao`, `id`, `data`, `qtd`, `und`) VALUES (1,'Gggh','566','Matriz','Ervilha','8','560,00','Desencaixada','De Boi','Medio Baixo','Ã  Combinar','Hgfhhj\n',3,'2019-01-12 10:20:15','','');
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'proig_bd'
--

--
-- Dumping routines for database 'proig_bd'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-25 16:14:09
