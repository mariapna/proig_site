﻿<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package crisecia
 */
	$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoBlog = $fotoBlog[0];

	global $post;
	$categories = get_the_category();
	$idPostAtual = $post->ID;
get_header();
?>
	<!-- PÁGINA DO POST -->
	<div class="pg pg-post">
		<style>header a img {margin-left: 9px;}</style>
		<div class="containerFull">
			<!-- IMAGEM DESTAQUE DO POST -->
			<div class="bannerPrincipalPost">
				<figure style="background: url(<?php echo $fotoBlog ?>);">
					<img src="<?php echo $fotoBlog ?>" alt="Banner principal Post">
				</figure>
			</div>

			<!-- ROW POST -->
			<div class="row">
				<div class="col-sm-8">
					<!-- INFORMAÇÕES DO POST -->
					<div class="informacoesPost">
						<!-- TÍTULO DO POST -->
						<h1 class="tituloPost"><?php echo get_the_title() ?></h1>
						<!-- CATEGORIA DO POST -->
						<?php 
							foreach ($categories as $categories){
								if ($categories->name != "destaque"){
									$nomeCategoria = $categories->name;
									$idCategoria = $categories->cat_ID;
								
								}
							} 

						?>
						<a href="<?php echo get_category_link( $idCategoria ) ?> " class="categoriaPost"><?php echo $nomeCategoria ?></a>
						<!-- DATA DO POST -->
						<span class="dataDoPost"><?php echo get_the_date('j  F  Y') ?></span>
					</div>

					<!-- CONTEUDO DO POST -->
					<div class="conteudoDoPost">
						<?php echo the_content() ?>
					</div>

					<!-- DISQUS -->
					<div class="disqus">
						<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>
					</div>
				</div>
			
				<div class="col-sm-4">
					<div class="sidebarSingle">
						<?php  get_sidebar(); ?>
						<!-- POSTS RELACIONADOS -->
						<div class="postsRelacionados">
							<span class="leiaTambem">Leia também</span>
							<?php dynamic_sidebar( 'sidebar-1' ); ?>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
