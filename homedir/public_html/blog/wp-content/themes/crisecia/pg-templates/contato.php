<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package crisecia
 */

get_header(); ?>
	<!-- PÁGINA DE CONTATO -->
	<div class="pg pg-contato">
		<!--CONTAINER BOOTSTRAP-->
		<div class="containerFull">
			<div class="formularioContato">
				<!-- <div class="row">
					<div class="col-md-6">
						<label for="nome">Nome</label>
						<input type="text" name="nome">

						<label for="telefone">Telefone</label>
						<input type="text" name="telefone">
					</div>
					<div class="col-md-6">
						<label for="email">Email</label>
						<input type="text" name="email">

						<label for="cnpj">Cnpj</label>
						<input type="text" name="cnpj">
					</div>

				</div>
				<label for="mensagem">Mensagem</label>
				<textarea name="mensagem" id="" cols="30" rows="10"></textarea>
				
				<input type="submit" name="enviar" value="Enviar"> -->
				 <?php echo do_shortcode('[contact-form-7 id="150" title="Formulário de contato"]'); ?>
			</div>
		</div>

		<section class="areaEndereco">
			<div class="infoContato">
				<div class="row">
					<div class="col-md-4">
						<div class="email">
							<div class="textos">
								<i class="far fa-envelope"></i>
								<span>Email</span>
								<p><?php echo $configuracao['opt_email_de_contato']; ?></p>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="horarios">
							<div class="textos">
								<i class="far fa-clock"></i>
								<span>Horários</span>
								<p><?php echo $configuracao['opt_horario_atendimento']; ?></p>
							</div>
						</div>
					</div>
						
					<div class="col-md-4">	
						<div class="telefones">
							<div class="textos">
								<i class="fas fa-phone"></i>
								<span>Contato</span>
								<p><?php echo $configuracao['opt_telefone']; ?></p>

								<i class="fas fa-map-marker-alt localizacao"></i>
								<span class="local">Local</span>
								<p><?php echo $configuracao['opt_endereco']; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="maps">
				<?php echo $configuracao['opt_iframe_maps']; ?>
			</div>
		</section>
	</div>

<?php get_footer(); ?>