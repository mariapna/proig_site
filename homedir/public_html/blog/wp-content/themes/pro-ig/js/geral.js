$(function(){


	/*****************************************
		SCRIPTS CARROSSEL DESTAQUE
	*******************************************/
		$("#carrosselDestaque").owlCarousel({
			items : 1,
	        dots: true,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    //responsiveClass:true,			    
	 //        responsive:{
	 //            320:{
	 //                items:1
	 //            },
	 //            600:{
	 //                items:2
	 //            },
	           
	 //            991:{
	 //                items:2
	 //            },
	 //            1024:{
	 //                items:3
	 //            },
	 //            1440:{
	 //                items:4
	 //            },
	            			            
	 //        }		    		   		    
		    
		});

	/*****************************************
		SCRIPTS CARROSSEL DE PRODUTOS
	*******************************************/
		$("#carrosselProdutosDestaque").owlCarousel({
			items : 4,
		    dots: true,
		    loop: true,
		    lazyLoad: true,
		    mouseDrag:true,
		    touchDrag  : true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
		    responsive:{
		         320:{
		             items:1
		        },
		         600:{
		             items:2
		        },
		       
		         991:{
		             items:3
		        },
		         1024:{
		             items:3
		        },
		         1200:{
		             items:3
		        },
		         1440:{
		             items:4
		        },
		      			            
		    }		    		   		    
		});
		    //BOTÕES DO CARROSSEL DE PARCERIA
			var carrosselProdutosDestaque = $("#carrosselProdutosDestaque").data('owlCarousel');
			$('#flechaEsquerda').click(function(){ carrosselProdutosDestaque.prev(); });
			$('#flechaDireita').click(function(){ carrosselProdutosDestaque.next(); });


	/*****************************************
		SCRIPTS CARROSSEL DE PRODUTOS
	*******************************************/
		$("#carrosselProdutosSidebar").owlCarousel({
			items : 1,
	        dots: true,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    //responsiveClass:true,			    
	 //        responsive:{
	 //            320:{
	 //                items:1
	 //            },
	 //            600:{
	 //                items:2
	 //            },
	           
	 //            991:{
	 //                items:2
	 //            },
	 //            1024:{
	 //                items:3
	 //            },
	 //            1440:{
	 //                items:4
	 //            },
	            			            
	 //        }		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL DE PARCERIA
		var carrosselProdutosSidebar = $("#carrosselProdutosSidebar").data('owlCarousel');
		$('.flechaEsquerdaCarrosselSidebar').click(function(){ carrosselProdutosSidebar.prev(); });
		$('.flechaDireitaCarrosselSidebar').click(function(){ carrosselProdutosSidebar.next(); });
			


	/*****************************************
		SCRIPTS MODAL NEWSLETTER
	*******************************************/
		$( "#abrirNewsletter" ).click(function( event ) {
	  		event.preventDefault();

			$('.lenteNewsletter').fadeIn();
			$('body').addClass('travarScroll');

			setTimeout(function(){ 
				$('.fecharModalNewsletter').fadeIn();
			}, 2000);
		});

		$( ".fecharModalNewsletter" ).click(function() {
			$('body').removeClass('travarScroll');
			$('.lenteNewsletter').fadeOut();
			$('.fecharModalNewsletter').fadeOut();	

		});

		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$('body').removeClass('travarScroll');
				$('.lenteNewsletter').fadeOut();
				$('.fecharModalNewsletter').fadeOut();
			}
		});


//	$(document).ready(function(){
//		
//	
//		//AJAX PARA FAZER A REQUISIÇÃO DE PRODUTOS
//		$.ajax({
//			type: "GET",
//			url: "https://api.fbits.net/produtos/",
//			dataType: 'json',
//			async: true,
//			data: '{}',
//
//			// TOLKEN DE ACESSO
//			beforeSend: function (xhr){
//				xhr.setRequestHeader('Authorization', 'Basic Crise-9edf33cd-26a9-4747-b496-bb45554ac5c2');
//			},
//
//			
//
//		}).done(function(produtos) {
 //   		
 //   		
//			// DEFININDO ÁREA DO LOOP
//			let sessaoProdutos = $('#carrosselProdutosDestaque')
//			
//			console.log(produtos);
//			//FOR PARA PERCORRER ARRAY DE PRODUTOS
//			for (var i = 0; i < produtos.length; i++) {
//				let produto = produtos[i];
//				$.ajax({
//					type: "GET",
//					url: "https://api.fbits.net/produtos/"+produto.sku+"/imagens?tipoIdentificador=Sku",
//					dataType: 'json',
//					async: true,
//					data: '{}',
//
//					// TOLKEN DE ACESSO
//					beforeSend: function (xhr){
//						xhr.setRequestHeader('Authorization', 'Basic Crise-9edf33cd-26a9-4747-b496-bb45554ac5c2');
//					},
//					// RESPOSTA SUCESSO
//					success: function (produtosImg){
//						
//						
//					}
//
//				
//				}).done(function(produtosImg) {
//
//					//TRATANDO EXCEÇÃO
//					try{
//						setTimeout(function(){ 
//						/*****************************************
//							SCRIPTS CARROSSEL DE PRODUTOS
//						*******************************************/
//						$("#carrosselProdutosDestaque").owlCarousel({
//							items : 4,
//					        dots: true,
//					        loop: false,
//					        lazyLoad: true,
//					        mouseDrag:true,
//					        touchDrag  : true,	       
//						    autoplayTimeout:5000,
//						    autoplayHoverPause:true,
//						    smartSpeed: 450,
//
//						    //CARROSSEL RESPONSIVO
//						    responsiveClass:true,			    
//					         responsive:{
//					             320:{
//					                 items:1
//					             },
//					             600:{
//					                 items:2
//					             },
//					           
//					             991:{
//					                 items:3
//					             },
//					             1024:{
//					                 items:3
//					             },
//					             1200:{
//					                 items:3
//					             },
//					             1440:{
//					                 items:4
//					             },
//					          			            
//					         }		    		   		    
//						});
//						    //BOTÕES DO CARROSSEL DE PARCERIA
//							var carrosselProdutosDestaque = $("#carrosselProdutosDestaque").data('owlCarousel');
//							$('#flechaEsquerda').click(function(){ carrosselProdutosDestaque.prev(); });
//							$('#flechaDireita').click(function(){ carrosselProdutosDestaque.next(); });
//
//						},1000);
//						sessaoProdutos.append( "<div class='item itemProdutoDestaque'><a href='"+ produto.urlProduto +"' target='_blank'><span class='porcentagemDesconto'>-18%</span><figure><img src='"+produtosImg[0]["url"]+"' alt=''></figure><h3>"+ produto.nome +"</h3><hr><span class='classificacaoProduto'><i class='fas fa-star'></i><i class='fas fa-star'></i><i class='fas fa-star'></i><i class='fas fa-star'></i><i class='fas fa-star'></i></span><small class='precoSemDesconto'>R$ "+ produto.precoDe.toString().replace(".", ",") +"</small><h4 class='precoAtual'><strong>R$ "+ produto.precoPor.toString().replace(".", ",") +"</strong> à vista</h4><small class='precoParcelado'>ou 6x de R$ 36,98 sem juros </small></a></div>" );
//					}
//					catch(error){
//						//console.log(produto);
//					}
//				});
//			
//			}
//				
//		});
//	
//	});

});