<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'proig_blog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'proig');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'josegol99@');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

define('WP_HOME','http://blog.proig.com.br/');
define('WP_SITEURL','http://blog.proig.com.br/');
define('RELOCATE',true);

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VamN;h|H5[-m0J5SKEly)y:sZy9enSGee{T^$Yb:pOp?[F?fAf7>it9JTn.]1al&');
define('SECURE_AUTH_KEY',  'MeO(Y:?zK`K0HPbV[bUOYVI+O_X]gV41aM,a>~T*;y/72BrDX+:>D7Pn4@]dF:#2');
define('LOGGED_IN_KEY',    ';-z/cxNLp#%2FTb@[;{?*-V3@>6e mekYB]4vq3odoa;G5a[{<d!^B?0~0]bb1W<');
define('NONCE_KEY',        'J zzVw~#_Na_WLmo9+,<BGDybD@~wtq#R=!hn5Z~C.xOL@scE>UT-7e`b|[F(4^3');
define('AUTH_SALT',        '9T6U8JIqzr?Y!gG_Qe0-3y*=3$_J~nRY]im=L/%M<Ll{@K`gDX~uP)FKCDVVWasW');
define('SECURE_AUTH_SALT', 'cRsR!7e[Rv2.SsKvH`V>sq7mb! t)F^=BR$zCLgL3%p|mw}std^NlEdC*puvrSmS');
define('LOGGED_IN_SALT',   ' ynik>6|g![g-~F0*]__nG7,X}P6cy!7M]bNym?>.096s~VbO,,cEp:6W)l,5K7=');
define('NONCE_SALT',       'U%pER2TZo6f#^z  }/j9y>}bIDH3.R8M;}~R)b3!`Pt{AK^tdYpdY2yY/m4x?/Lp');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cs_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
