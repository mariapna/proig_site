<?php

/* settings/mta.html */
class __TwigTemplate_639e2b06c1d7cdd573959eecad2deb132319f0657131cbcbb019ea20b1f1b3dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["intervals"] = array(0 => 1, 1 => 2, 2 => 5, 3 => 10, 4 => 15, 5 => 30);
        // line 2
        $context["default_frequency"] = array("website" => array("emails" => 25, "interval" => 5), "smtp" => array("emails" => 100, "interval" => 5));
        // line 12
        echo "
<!-- mta: group -->
<input
  type=\"hidden\"
  id=\"mta_group\"
  name=\"mta_group\"
  value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["settings"] ?? null), "mta_group", array()), "html", null, true);
        echo "\"
/>
<input
  type=\"hidden\"
  id=\"mailpoet_smtp_provider\"
  name=\"mailpoet_smtp_provider\"
  value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()), "html", null, true);
        echo "\"
/>
<!-- mta: method -->
<input
  type=\"hidden\"
  id=\"mta_method\"
  name=\"mta[method]\"
  value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "method", array()), "html", null, true);
        echo "\"
/>

<!-- mta: sending frequency -->
<input
  type=\"hidden\"
  id=\"mta_frequency_emails\"
  name=\"mta[frequency][emails]\"
  value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "emails", array()), "html", null, true);
        echo "\"
/>
<input
  type=\"hidden\"
  id=\"mta_frequency_interval\"
  name=\"mta[frequency][interval]\"
  value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "interval", array()), "html", null, true);
        echo "\"
/>

<!-- mta: mailpoet sending service key -->
<input
  type=\"hidden\"
  id=\"mailpoet_api_key\"
  name=\"mta[mailpoet_api_key]\"
  value=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "mailpoet_api_key", array()), "html", null, true);
        echo "\"
/>

<!-- smtp: available sending methods -->
<ul class=\"mailpoet_sending_methods\">
  <li
    data-group=\"mailpoet\"
    class=\"";
        // line 60
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "mailpoet")) {
            echo " mailpoet_active ";
        }
        echo " ";
        if ( !($context["mss_key_valid"] ?? null)) {
            echo " mailpoet_invalid_key ";
        }
        echo "\"
  >
    <div class=\"mailpoet_sending_method_description\">
      <h3>
        ";
        // line 64
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("MailPoet Sending Service");
        echo "
      </h3>

      <div class=\"mailpoet_description\">
        <span class=\"";
        // line 68
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "mailpoet")) {
            echo " mailpoet_hidden";
        }
        echo "\" id=\"mailpoet_sending_method_active_text\">
          <strong>";
        // line 69
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("You're now sending with MailPoet!");
        echo "</strong>
        </span>

        <span class=\"";
        // line 72
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "mailpoet")) {
            echo " mailpoet_hidden";
        }
        echo "\" id=\"mailpoet_sending_method_inactive_text\">
          <strong>";
        // line 73
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Solve all of your sending problems!");
        echo "</strong>
        </span>

        <ul class=\"sending-method-benefits mailpoet_success\">
          <li class=\"mailpoet_success_item\">";
        // line 77
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Reach the inbox, not the spam box.");
        echo "</li>
          <li class=\"mailpoet_success_item\">";
        // line 78
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Easy configuration: enter a key to activate the sending service.");
        echo "</li>
          <li class=\"mailpoet_success_item\">";
        // line 79
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Super fast: send up to 50,000 emails per hour.");
        echo "</li>
          <li class=\"mailpoet_success_item\">";
        // line 80
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("All emails are signed with SPF & DKIM.");
        echo "</li>
          <li class=\"mailpoet_success_item\">";
        // line 81
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Automatically remove invalid and bounced addresses (bounce handling) to keep your lists clean.");
        echo "</li>
        </ul>

        <p class=\"mailpoet_sending_methods_help\">
          ";
        // line 85
        $context["badgeClassName"] = (((($context["is_new_user"] ?? null) == true)) ? ("mailpoet_badge mailpoet_badge_video") : ("mailpoet_badge mailpoet_badge_video mailpoet_badge_video_grey"));
        // line 86
        echo "          ";
        echo MailPoet\Util\Helpers::replaceLinkTags(sprintf($this->env->getExtension('MailPoet\Twig\I18n')->translate("[link]%s See video guide[/link]"), "<span class=\"dashicons dashicons-format-video\"></span>"), "https://beta.docs.mailpoet.com/article/235-video-guide-sending-options", array("class" =>         // line 88
($context["badgeClassName"] ?? null), "target" => "_blank"), "link");
        // line 90
        echo "
        </p>
      </div>
    </div>
    <div class=\"mailpoet_status ";
        // line 94
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "mailpoet") ||  !($context["mss_key_valid"] ?? null))) {
            echo " mailpoet_invalid_key ";
        }
        echo "\">
      <div class=\"mailpoet_activated ";
        // line 95
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "mailpoet") ||  !($context["mss_key_valid"] ?? null))) {
            echo " mailpoet_hidden ";
        }
        echo "\">
        <span>";
        // line 96
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activated");
        echo "</span>
      </div>
      <div class=\"mailpoet_actions\">
        <div class=\"mailpoet_invalid_key ";
        // line 99
        if (($context["mss_key_valid"] ?? null)) {
            echo " mailpoet_hidden ";
        }
        echo "\">
          <a class=\"button-primary\" href=\"https://www.mailpoet.com/free-plan/\" rel=\"noopener noreferrer\" target=\"_blank\">";
        // line 100
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Get a free key");
        echo "</a>
          ";
        // line 101
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("or");
        echo "
          ";
        // line 102
        echo MailPoet\Util\Helpers::replaceLinkTags($this->env->getExtension('MailPoet\Twig\I18n')->translate("[link]enter your key[/link]"), "#premium");
        // line 106
        echo "
        </div>
        <div class=\"mailpoet_valid_key ";
        // line 108
        if (( !($context["mss_key_valid"] ?? null) || ($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "mailpoet"))) {
            echo " mailpoet_hidden ";
        }
        echo "\">
          <button type=\"button\" class=\"mailpoet_sending_service_activate button-primary\">";
        // line 109
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activate");
        echo "</button>
        </div>
      </div>
  </li>
  <li
    data-group=\"other\"
    ";
        // line 115
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "mailpoet")) {
            echo "class=\"mailpoet_active\"";
        }
        // line 116
        echo "  >
    <div class=\"mailpoet_sending_method_description\">
      <h3>";
        // line 118
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Other");
        echo "</h3>
      <div class=\"mailpoet_description\">
        <strong>";
        // line 120
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Send emails via your host (not recommended!) or via a third-party sender.");
        echo "</strong>
        <ul class=\"sending-method-benefits mailpoet_error\">
          <li class=\"mailpoet_error_item\">";
        // line 122
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Unless you're a pro, you’ll probably end up in spam.");
        echo "</li>
          <li class=\"mailpoet_error_item\">";
        // line 123
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Sending speed is limited by your host and/or your third-party (with a 2,000 per hour maximum).");
        echo "</li>
          <li class=\"mailpoet_error_item\">";
        // line 124
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Manual configuration of SPF and DKIM required.");
        echo "</li>
          <li class=\"mailpoet_error_item\">";
        // line 125
        echo MailPoet\Util\Helpers::replaceLinkTags($this->env->getExtension('MailPoet\Twig\I18n')->translate("Bounce handling is available, but only with an extra [link]add-on[/link]."), "https://wordpress.org/plugins/bounce-handler-mailpoet/", array("target" => "_blank"));
        // line 129
        echo "
          </li>
        </ul>
      </div>
    </div>
    <div class=\"mailpoet_status\">
      <span>";
        // line 135
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activated");
        echo "</span>
      <div class=\"mailpoet_actions\">
        ";
        // line 137
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "mailpoet")) {
            // line 138
            echo "          <a class=\"button-primary mailpoet_other_sending_method_action\" href=\"#mta/other\">";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activate");
            echo "</a>
        ";
        } else {
            // line 140
            echo "          <a class=\"button-secondary mailpoet_other_sending_method_action\" href=\"#mta/other\">";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Configure");
            echo "</a>
        ";
        }
        // line 142
        echo "      </div>
    </div>
  </li>
</ul>

<div id=\"mailpoet_sending_method_setup\">

  <!-- Sending Method: Other -->
  <div class=\"mailpoet_sending_method\" data-group=\"other\" style=\"display:none;\">
    <table class=\"form-table\">
      <tr>
        <th scope=\"row\">
          <label for=\"mailpoet_smtp_method\">
            ";
        // line 155
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Method");
        echo "
          </label>
        </th>
        <td>
          <!-- smtp provider -->
          <select
            id=\"mailpoet_smtp_method\"
            name=\"smtp_provider\"
          >
            <option data-interval=\"5\" data-emails=\"25\" value=\"server\">
              ";
        // line 165
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your web host / web server");
        echo "
            </option>
            <option data-interval=\"5\" data-emails=\"100\" value=\"manual\"
              ";
        // line 169
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "smtp")) {
            // line 171
            echo "              selected=\"selected\"
              ";
        }
        // line 173
        echo "            >
              ";
        // line 174
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("SMTP");
        echo "
            </option>
            <!-- providers -->
            <optgroup label=\"";
        // line 177
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Select your provider");
        echo "\">
              ";
        // line 178
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["hosts"] ?? null), "smtp", array()));
        foreach ($context['_seq'] as $context["host_key"] => $context["host"]) {
            // line 179
            echo "              <option
                value=\"";
            // line 180
            echo twig_escape_filter($this->env, $context["host_key"], "html", null, true);
            echo "\"
                data-emails=\"";
            // line 181
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "emails", array()), "html", null, true);
            echo "\"
                data-interval=\"";
            // line 182
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "interval", array()), "html", null, true);
            echo "\"
                data-fields=\"";
            // line 183
            echo twig_escape_filter($this->env, twig_join_filter($this->getAttribute($context["host"], "fields", array()), ","), "html", null, true);
            echo "\"
              ";
            // line 184
            if (($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) == $context["host_key"])) {
                // line 185
                echo "              selected=\"selected\"
              ";
            }
            // line 187
            echo "              >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "name", array()), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['host_key'], $context['host'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "            </optgroup>
          </select>
        </td>
      </tr>
      <tr id=\"mailpoet_sending_method_host\"
        ";
        // line 195
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "smtp")) {
            // line 197
            echo "        style=\"display:none;\"
        ";
        }
        // line 199
        echo "      >
        <th scope=\"row\">
          <label for=\"mailpoet_web_host\">
            ";
        // line 202
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Your web host");
        echo "
          </label>
        </th>
        <td>
          <p>
            <!-- sending frequency -->
            <select
              id=\"mailpoet_web_host\"
              name=\"web_host\"
            >

              <!-- web hosts -->
              <option
                value=\"manual\"
                data-emails=\"25\"
                data-interval=\"5\"
                label=\"";
        // line 218
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Not listed (default)");
        echo "\"
              >
              ";
        // line 220
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["hosts"] ?? null), "web", array()));
        foreach ($context['_seq'] as $context["host_key"] => $context["host"]) {
            // line 221
            echo "              <option
                value=\"";
            // line 222
            echo twig_escape_filter($this->env, $context["host_key"], "html", null, true);
            echo "\"
                data-emails=\"";
            // line 223
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "emails", array()), "html", null, true);
            echo "\"
                data-interval=\"";
            // line 224
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "interval", array()), "html", null, true);
            echo "\"
              ";
            // line 225
            if (($this->getAttribute(($context["settings"] ?? null), "web_host", array()) == $context["host_key"])) {
                // line 226
                echo "              selected=\"selected\"
              ";
            }
            // line 228
            echo "              >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["host"], "name", array()), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['host_key'], $context['host'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 230
        echo "            </select>
          </p>

        </td>
      </tr>
      <tr>
        <th scope=\"row\">
          <label for=\"mailpoet_web_host\">
            ";
        // line 238
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Sending frequency");
        echo "
          </label>
        </th>
        <td>
          <p>
            <!-- sending frequency -->
            <select
              id=\"mailpoet_sending_frequency\"
              name=\"mailpoet_sending_frequency\"
            >
              <option value=\"auto\">
                ";
        // line 249
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Recommended");
        echo "
              </option>
              <option value=\"manual\"
                ";
        // line 253
        if ( !($this->getAttribute(($context["settings"] ?? null), "mailpoet_sending_frequency", array()) == "auto")) {
            // line 255
            echo "                selected=\"selected\"
                ";
        }
        // line 257
        echo "              >
                ";
        // line 258
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("I'll set my own frequency");
        echo "
              </option>
            </select>
            <span id=\"mailpoet_recommended_daily_emails\"></span>
          </p>
          <div id=\"mailpoet_sending_frequency_manual\"
            ";
        // line 265
        if (($this->getAttribute(($context["settings"] ?? null), "mailpoet_sending_frequency", array()) == "auto")) {
            // line 267
            echo "              style=\"display: none\"
            ";
        }
        // line 269
        echo "          >
            <p>
              <input
                id=\"other_frequency_emails\"
                type=\"number\"
                min=\"1\"
                max=\"1000\"
              ";
        // line 276
        if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "emails", array())) {
            // line 277
            echo "                value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "emails", array()), "html", null, true);
            echo "\"
              ";
        } else {
            // line 279
            echo "                value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["default_frequency"] ?? null), "website", array()), "emails", array()), "html", null, true);
            echo "\"
              ";
        }
        // line 281
        echo "              />
              ";
        // line 282
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("emails");
        echo "
              <select id=\"other_frequency_interval\">
                ";
        // line 284
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["intervals"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["interval"]) {
            // line 285
            echo "                <option
                  value=\"";
            // line 286
            echo twig_escape_filter($this->env, $context["interval"], "html", null, true);
            echo "\"
                ";
            // line 288
            if (( !$this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "interval", array()) && (            // line 289
$context["interval"] == $this->getAttribute($this->getAttribute(($context["default_frequency"] ?? null), "website", array()), "interval", array())))) {
                // line 291
                echo "                selected=\"selected\"
                ";
            }
            // line 293
            echo "                ";
            if (($this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "frequency", array()), "interval", array()) == $context["interval"])) {
                // line 294
                echo "                selected=\"selected\"
                ";
            }
            // line 296
            echo "                >
                ";
            // line 297
            echo $this->env->getExtension('MailPoet\Twig\Functions')->getSendingFrequency($context["interval"]);
            echo "
                ";
            // line 298
            if (($context["interval"] == $this->getAttribute($this->getAttribute(($context["default_frequency"] ?? null), "website", array()), "interval", array()))) {
                // line 299
                echo "                (";
                echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("recommended");
                echo ")
                ";
            }
            // line 301
            echo "                </option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interval'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 303
        echo "              </select>
              <span id=\"mailpoet_other_daily_emails\"></span>
            </p>
            <br />
            <p>
              ";
        // line 308
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("<strong>Warning!</strong> You may break the terms of your web host or provider by sending more than the recommended emails per day. Contact your host if you want to send more.");
        echo "
            </p>
          </div>
        </td>
      </tr>
      <tr class=\"mailpoet_smtp_field\" data-field=\"host\"
        ";
        // line 315
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 317
            echo "        style=\"display:none;\"
        ";
        }
        // line 319
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_host]\">
            ";
        // line 322
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("SMTP Hostname");
        echo "
          </label>
          <p class=\"description\">
            ";
        // line 325
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("e.g.: smtp.mydomain.com");
        echo "
          </p>
        </th>
        <td>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"settings[mta_host]\"
            name=\"mta[host]\"
            value=\"";
        // line 334
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "host", array()), "html", null, true);
        echo "\" />
        </td>
      </tr>
      <!-- smtp: port -->
      <tr class=\"mailpoet_smtp_field\" data-field=\"port\"
        ";
        // line 340
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 342
            echo "        style=\"display:none;\"
        ";
        }
        // line 344
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_port]\">
            ";
        // line 347
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("SMTP Port");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"number\"
            max=\"65535\"
            min=\"1\"
            maxlength=\"5\"
            style=\"width:5em;\"
            id=\"settings[mta_port]\"
            name=\"mta[port]\"
            value=\"";
        // line 359
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "port", array()), "html", null, true);
        echo "\"
          />
        </td>
      </tr>

      <!-- smtp: amazon region -->
      <tr class=\"mailpoet_aws_field\" data-field=\"region\"
        ";
        // line 367
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "AmazonSES"))) {
            // line 369
            echo "        style=\"display:none;\"
        ";
        }
        // line 371
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_region]\">
            ";
        // line 374
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Region");
        echo "
          </label>
        </th>
        <td>
          <select
            id=\"settings[mta_region]\"
            name=\"mta[region]\"
            value=\"";
        // line 381
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "smtp")) {
            // line 382
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "region", array()), "html", null, true);
        }
        // line 383
        echo "\"
          >
            ";
        // line 385
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["hosts"] ?? null), "smtp", array()), "AmazonSES", array()), "regions", array()));
        foreach ($context['_seq'] as $context["region"] => $context["server"]) {
            // line 386
            echo "            <option
              value=\"";
            // line 387
            echo twig_escape_filter($this->env, $context["server"], "html", null, true);
            echo "\"
            ";
            // line 388
            if (($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "region", array()) == $context["server"])) {
                // line 389
                echo "            selected=\"selected\"
            ";
            }
            // line 391
            echo "            >
            ";
            // line 392
            echo twig_escape_filter($this->env, $context["region"], "html", null, true);
            echo "
            </option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['region'], $context['server'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 395
        echo "          </select>
        </td>
      </tr>

      <!-- smtp: amazon access_key -->
      <tr class=\"mailpoet_aws_field\" data-field=\"access_key\"
        ";
        // line 402
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "AmazonSES"))) {
            // line 404
            echo "        style=\"display:none;\"
        ";
        }
        // line 406
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_access_key]\">
            ";
        // line 409
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Access Key");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"settings[mta_access_key]\"

            name=\"mta[access_key]\"
            value=\"";
        // line 419
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "smtp")) {
            // line 420
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "access_key", array()), "html", null, true);
        }
        // line 421
        echo "\"
          />
        </td>
      </tr>

      <!-- smtp: amazon secret_key -->
      <tr class=\"mailpoet_aws_field\" data-field=\"secret_key\"
        ";
        // line 429
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "AmazonSES"))) {
            // line 431
            echo "        style=\"display:none;\"
        ";
        }
        // line 433
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_secret_key]\">
            ";
        // line 436
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Secret Key");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"settings[mta_secret_key]\"

            name=\"mta[secret_key]\"
            value=\"";
        // line 446
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) == "smtp")) {
            // line 447
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "secret_key", array()), "html", null, true);
        }
        // line 448
        echo "\"
          />
        </td>
      </tr>

      <!-- smtp: api key -->
      <tr class=\"mailpoet_sendgrid_field\" data-field=\"api_key\"
        ";
        // line 456
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "SendGrid"))) {
            // line 458
            echo "        style=\"display:none;\"
        ";
        }
        // line 460
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_api_key]\">
            ";
        // line 463
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("API Key");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"settings[mta_api_key]\"
            name=\"mta[api_key]\"
            value=\"";
        // line 472
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "api_key", array()), "html", null, true);
        echo "\"
          />
        </td>
      </tr>

      <!-- smtp: login -->
      <tr id=\"mta_login\" class=\"mailpoet_smtp_field\" data-field=\"login\"
        ";
        // line 480
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 482
            echo "        style=\"display:none;\"
        ";
        }
        // line 484
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_login]\">
            ";
        // line 487
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Login");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"text\"
            class=\"regular-text\"
            id=\"settings[mta_login]\"
            name=\"mta[login]\"
            value=\"";
        // line 496
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "login", array()), "html", null, true);
        echo "\"
          />
        </td>
      </tr>
      <!-- smtp: password -->
      <tr id=\"mta_password\" class=\"mailpoet_smtp_field\" data-field=\"password\"
        ";
        // line 503
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 505
            echo "        style=\"display:none;\"
        ";
        }
        // line 507
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_password]\">
            ";
        // line 510
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Password");
        echo "
          </label>
        </th>
        <td>
          <input
            type=\"password\"
            class=\"regular-text\"
            id=\"settings[mta_password]\"
            name=\"mta[password]\"
            value=\"";
        // line 519
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "password", array()), "html", null, true);
        echo "\"
          />
        </td>
      </tr>
      <!-- smtp: security protocol -->
      <tr class=\"mailpoet_smtp_field\" data-field=\"encryption\"
        ";
        // line 526
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 528
            echo "        style=\"display:none;\"
        ";
        }
        // line 530
        echo "      >
        <th scope=\"row\">
          <label for=\"settings[mta_encryption]\">
            ";
        // line 533
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Secure Connection");
        echo "
          </label>
        </th>
        <td>
          <select id=\"settings[mta_encryption]\" name=\"mta[encryption]\">
            <option value=\"\">";
        // line 538
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("No");
        echo "</option>
            <option
              value=\"ssl\"
            ";
        // line 541
        if (($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "encryption", array()) == "ssl")) {
            // line 542
            echo "            selected=\"selected\"
            ";
        }
        // line 544
        echo "            >SSL</option>
            <option
              value=\"tls\"
            ";
        // line 547
        if (($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "encryption", array()) == "tls")) {
            // line 548
            echo "            selected=\"selected\"
            ";
        }
        // line 550
        echo "            >TLS</option>
          </select>
        </td>
      </tr>
      <!-- smtp: authentication -->
      <tr class=\"mailpoet_smtp_field\" data-field=\"authentication\"
        ";
        // line 557
        if ((($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "smtp") || ($this->getAttribute(($context["settings"] ?? null), "smtp_provider", array()) != "manual"))) {
            // line 559
            echo "        style=\"display:none;\"
        ";
        }
        // line 561
        echo "      >
        <th scope=\"row\">
          <label>
            ";
        // line 564
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Authentication");
        echo "
          </label>
          <p class=\"description\">
            ";
        // line 567
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Leave this option set to Yes. Only a tiny portion of SMTP services prefer Authentication to be turned off.");
        echo "
          </p>
        </th>
        <td>
          <label>
            <input
              type=\"radio\"
              value=\"1\"
              name=\"mta[authentication]\"
            ";
        // line 577
        if (( !$this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "authentication", array()) || ($this->getAttribute($this->getAttribute(        // line 578
($context["settings"] ?? null), "mta", array()), "authentication", array()) == "1"))) {
            // line 579
            echo "            checked=\"checked\"
            ";
        }
        // line 581
        echo "            />";
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yes");
        echo "
          </label>
          &nbsp;
          <label>
            <input
              type=\"radio\"
              value=\"-1\"
              name=\"mta[authentication]\"
            ";
        // line 589
        if (($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mta", array()), "authentication", array()) == "-1")) {
            // line 590
            echo "            checked=\"checked\"
            ";
        }
        // line 592
        echo "            />";
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("No");
        echo "
          </label>
        </td>
      </tr>
      </tbody>
    </table>

    <table class=\"form-table\">
      <tbody>
        <!-- SPF -->
        <tr id=\"mailpoet_mta_spf\">
          <th scope=\"row\">
            <label>
              ";
        // line 605
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("SPF Signature (Highly recommended!)");
        echo "
            </label>
            <p class=\"description\">
              ";
        // line 608
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("This improves your delivery rate by verifying that you're allowed to send emails from your domain.");
        echo "
            </p>
          </th>
          <td>
            <p>
              ";
        // line 613
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("SPF is set up in your DNS. Read your host's support documentation for more information.");
        echo "
            </p>
          </td>
        </tr>
        <!-- test method -->
        <tr>
          <th scope=\"row\">
            <label for=\"mailpoet_mta_test_email\">
              ";
        // line 621
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Test the sending method");
        echo "
            </label>
          </th>
          <td>
            <p>
              <input
                type=\"text\"
                id=\"mailpoet_mta_test_email\"
                class=\"regular-text\"
                value=\"";
        // line 630
        echo twig_escape_filter($this->env, $this->getAttribute(($context["current_user"] ?? null), "user_email", array()), "html", null, true);
        echo "\"
              />
              <a
                id=\"mailpoet_mta_test\"
                class=\"button-secondary\"
              >";
        // line 635
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Send a test email");
        echo "</a>

              <span id=\"tooltip-test\"></span>
            </p>
          </td>
        </tr>
        <!-- activate or cancel -->
        <tr>
          <th scope=\"row\">
            <p>
              <a
                href=\"javascript:;\"
                class=\"mailpoet_mta_setup_save button button-primary\"
              >";
        // line 648
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activate");
        echo "</a>
              &nbsp;
              <a
                href=\"javascript:;\"
                class=\"mailpoet_mta_setup_cancel\"
              >";
        // line 653
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("or Cancel");
        echo "</a>
            </p>
          </th>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<script type=\"text/javascript\">
  jQuery(function(\$) {
    var tooltip = '";
        // line 665
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, MailPoet\Util\Helpers::replaceLinkTags($this->env->getExtension('MailPoet\Twig\I18n')->translate("Didn't receive the test email? Read our [link]quick guide[/link] to sending issues."), "http://beta.docs.mailpoet.com/article/146-my-newsletters-are-not-being-received", array("target" => "_blank")), "js"), "html", null, true);
        // line 667
        echo "'

    MailPoet.helpTooltip.show(document.getElementById(\"tooltip-test\"), {
      tooltipId: \"tooltip-settings-test\",
      tooltip: tooltip,
    });

    var sending_frequency_template =
      Handlebars.compile(\$('#mailpoet_sending_frequency_template').html());

    // om dom loaded
    \$(function() {
      // constrain number type inputs
      \$('input[type=\"number\"]').on('keyup', function() {
        var currentValue = \$(this).val();
        if(!currentValue) return;

        var minValue = \$(this).attr('min');
        var maxValue = \$(this).attr('max');
        \$(this).val(Math.min(Math.max(minValue, currentValue), maxValue));
      });

      // testing sending method
      \$('#mailpoet_mta_test').on('click', function() {
        // get test email and include it in data
        var recipient = \$('#mailpoet_mta_test_email').val();

        var settings = jQuery('#mailpoet_settings_form').mailpoetSerializeObject();
        var mailer = settings.mta;

        mailer.method = getMethodFromGroup(\$('#mailpoet_smtp_method').val());

        // check that we have a from address
        if(settings.sender.address.length === 0) {
          // validation
          return MailPoet.Notice.error(
            '";
        // line 703
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('MailPoet\Twig\I18n')->translate("The email could not be sent. Make sure the option \"Email notifications\" has a FROM email address in the Basics tab."), "js"), "html", null, true);
        echo "',
            { scroll: true, static: true }
          );
        }

        MailPoet.Modal.loading(true);
        MailPoet.Ajax.post({
          api_version: window.mailpoet_api_version,
          endpoint: 'mailer',
          action: 'send',
          data: {
            mailer: mailer,
            newsletter: {
              subject: \"";
        // line 716
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("This is a Sending Method Test");
        echo "\",
              body: {
                html: \"<p>";
        // line 718
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yup, it works! You can start blasting away emails to the moon.");
        echo "</p>\",
                text: \"";
        // line 719
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Yup, it works! You can start blasting away emails to the moon.");
        echo "\"
              }
            },
            subscriber: recipient
          }
        }).always(function() {
          MailPoet.Modal.loading(false);
        }).done(function(response) {
          MailPoet.Notice.success(
            \"";
        // line 728
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('MailPoet\Twig\I18n')->translate("The email has been sent! Check your inbox."), "js"), "html", null, true);
        echo "\",
            { scroll: true }
          );
          trackTestEmailSent(mailer, true);

          const showSuccessDeliveryPoll = MailPoet.Poll.successDelivery.canShow('preview', true) && \$('#mailpoet_smtp_method').val() === 'server';
          if (showSuccessDeliveryPoll) {
            MailPoet.Poll.successDelivery.showModal('preview', 'IHedf1');
            MailPoet.Poll.successDelivery.setPollShown('preview');
          }
        }).fail(function(response) {
          if (response.errors.length > 0) {
            MailPoet.Notice.error(
              response.errors.map(function(error) { return _.escape(error.message); }),
              { scroll: true }
            );
          }
          trackTestEmailSent(mailer, false);
        });
      });

      // sending frequency update based on selected provider
      \$('#mailpoet_web_host').on('change keyup', renderHostSendingFrequency);

      // update manual sending frequency when values are changed
      \$('#other_frequency_emails').on('change keyup', function() {
        updateSendingFrequency('other');
      });
      \$('#other_frequency_interval').on('change keyup', function() {
        updateSendingFrequency('other');
      });

      // save configuration of a sending method
      \$('.mailpoet_sending_service_activate').on('click', function(e, navigateToTab) {
        \$('#mta_group').val('mailpoet');
        saveSendingMethodConfiguration('mailpoet', navigateToTab);
      });
      \$('.mailpoet_mta_setup_save').on('click', function() {
        \$('#mailpoet_smtp_method').trigger(\"change\");
        \$('#other_frequency_emails').trigger(\"change\");
        // get selected method
        var group = \$('.mailpoet_sending_method:visible').data('group');
        saveSendingMethodConfiguration(group);
      });
      \$('#mailpoet_smtp_method').on('change keyup', renderHostsSelect);
      \$('#mailpoet_sending_frequency').on('change keyup', sendingFrequencyMethodUpdated);

      ";
        // line 775
        if (($this->getAttribute(($context["settings"] ?? null), "mta_group", array()) != "mailpoet")) {
            // line 776
            echo "        \$('#mailpoet_smtp_method').trigger(\"change\");
        \$('#other_frequency_emails').trigger(\"change\");
      ";
        }
        // line 779
        echo "
      function saveSendingMethodConfiguration(group, navigateToTab) {
        // set sending method
        if(group === undefined) {
          MailPoet.Notice.error(
            \"";
        // line 784
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('MailPoet\Twig\I18n')->translate("You have selected an invalid sending method."), "js"), "html", null, true);
        echo "\"
          );
        } else {
          // set new sending method active
          setSendingMethodGroup(group);

          // enforce signup confirmation depending on selected group
          setSignupConfirmation(group);

          // back to selection of sending methods
          if (navigateToTab === undefined || navigateToTab === true) {
            MailPoet.Router.navigate('mta', { trigger: true });
          }

          // save settings
          \$('.mailpoet_settings_submit > input').trigger('click');

          if (group === 'mailpoet') {
            updateMSSActivationUI()
          }
        }
      }

      function setSignupConfirmation(group) {
        if (group === 'mailpoet') {
          // force signup confirmation (select \"Yes\")
          \$('.mailpoet_signup_confirmation[value=\"1\"]').attr('checked', true);
          \$('.mailpoet_signup_confirmation[value=\"\"]').attr('checked', false);

          // hide radio inputs
          \$('#mailpoet_signup_confirmation_input').hide();

          // show mailpoet specific notice
          \$('#mailpoet_signup_confirmation_notice').show();

          // show signup confirmation options
          \$('#mailpoet_signup_options').show();
        } else {
          // show radio inputs
          \$('#mailpoet_signup_confirmation_input').show();

          // hide mailpoet specific notice
          \$('#mailpoet_signup_confirmation_notice').hide();
        }
      }

      function setSendingMethodGroup(group) {
        // deactivate other sending methods
        \$('.mailpoet_sending_methods .mailpoet_active')
          .removeClass('mailpoet_active');

        // set active sending method
        \$('.mailpoet_sending_methods li[data-group=\"'+group+'\"]')
          .addClass('mailpoet_active');

        var method = getMethodFromGroup(\$('#mta_group').val());

        \$('#mta_method').val(method);

        // set MailPoet method description
        \$('#mailpoet_sending_method_active_text')
          .toggleClass('mailpoet_hidden', group !== 'mailpoet');
        \$('#mailpoet_sending_method_inactive_text')
          .toggleClass('mailpoet_hidden', group === 'mailpoet');

        // Hide server error notices
        \$('.mailpoet_notice_server').hide();

        updateMSSActivationUI();
      }

      function getMethodFromGroup(group) {
        var group = group || 'website';

        switch(group) {
          case 'mailpoet':
            return 'MailPoet';
          break;
          case 'server':
          case 'website':
            return 'PHPMail';
          break;
          case 'manual':
          case 'smtp':
            var method = \$('#mailpoet_smtp_provider').val();
            if(method === 'manual') {
              return 'SMTP';
            }
            return method;
          break;
          default:
            return group;
        }
      }

      // cancel configuration of a sending method
      \$('.mailpoet_mta_setup_cancel').on('click', function() {
        // back to selection of sending methods
        MailPoet.Router.navigate('mta', { trigger: true });
      });

      // render sending frequency form
      \$('#mailpoet_smtp_provider').trigger('change');
      \$('#mailpoet_web_host').trigger('change');

      function trackTestEmailSent(mailer, success) {
        MailPoet.trackEvent(
          'User has sent a test email from Settings',
          {
            'Sending was successful': !!success,
            'Sending method type': mailer.method,
            'MailPoet Free version': window.mailpoet_version
          }
        );
      }

      \$('.mailpoet_sending_methods_help a').on('click', function() {
        MailPoet.trackEvent(
          'User has clicked to view the sending comparison table',
          {'MailPoet Free version': window.mailpoet_version}
        );
      });
    });

    function setProviderForm() {
      // check provider
      var provider = \$(this).find('option:selected').first();
      var fields = provider.data('fields');

      if(fields === undefined) {
        fields = [
          'host',
          'port',
          'login',
          'password',
          'authentication',
          'encryption'
        ];
      } else {
        fields = fields.split(',');
      }

      \$('.mailpoet_smtp_field').hide();

      fields.map(function(field) {
        \$('.mailpoet_smtp_field[data-field=\"'+field+'\"]').show();
      });

      // update sending frequency
      renderSMTPSendingFrequency(provider);
    }

    function renderSMTPSendingFrequency() {
      // set sending frequency
      var emails = \$('#smtp_frequency_emails').val();
      var interval = \$('#smtp_frequency_interval').val();
      setSendingFrequency({
        output: '#mailpoet_smtp_daily_emails',
        only_daily: true,
        emails: emails,
        interval: interval
      });
      \$('#mta_frequency_emails').val(emails);
      \$('#mta_frequency_interval').val(interval);
    }

    function sendingFrequencyMethodUpdated() {
      var method = \$(this).find('option:selected').first();
      var sendingMethod = \$('#mailpoet_smtp_method').find('option:selected').first().val();
      if(method.val() === \"manual\") {
        \$('#mailpoet_sending_frequency_manual').show();
        \$('#mailpoet_recommended_daily_emails').hide();
        \$('#other_frequency_emails').trigger(\"change\");
      } else {
        \$('#mailpoet_sending_frequency_manual').hide();
        if(sendingMethod !== \"server\") {
          \$('#mailpoet_recommended_daily_emails').show();
        }
        \$('#mailpoet_smtp_method').trigger(\"change\");
      }
    }

    function renderHostsSelect() {
      var method = \$(this).find('option:selected').first();
      var val = method.val();

      if(val === \"server\") {
        \$('#mailpoet_sending_method_host').show();
        \$('#mta_group').val('website');
      } else {
        \$('#mailpoet_sending_method_host').hide();
      }
      if(val === \"manual\") {
        \$('.mailpoet_smtp_field').show();
        \$('#mta_group').val('smtp');
        \$('#mailpoet_smtp_provider').val('manual');
      } else {
        \$('.mailpoet_smtp_field').hide();
      }
      if(val === \"AmazonSES\") {
        \$('.mailpoet_aws_field').show();
        \$('#mta_group').val('smtp');
        \$('#mailpoet_smtp_provider').val('AmazonSES');
      } else {
        \$('.mailpoet_aws_field').hide();
      }
      if(val === \"SendGrid\") {
        \$('.mailpoet_sendgrid_field').show();
        \$('#mta_group').val('smtp');
        \$('#mailpoet_smtp_provider').val('SendGrid');
      } else {
        \$('.mailpoet_sendgrid_field').hide();
      }
      var emails = method.data('emails');
      var interval = method.data('interval');
      if(val === \"server\") {
        emails = \$('#mailpoet_web_host').find('option:selected').first().data('emails');
        interval = \$('#mailpoet_web_host').find('option:selected').first().data('interval');
      }
      const frequencyMethod = \$('#mailpoet_sending_frequency').find('option:selected').first().val();
      if(frequencyMethod === \"manual\") {
        \$('#mailpoet_recommended_daily_emails').hide();
        emails = \$('#other_frequency_emails').val();
        interval = \$('#other_frequency_interval').val();
      } else {
        \$('#mailpoet_recommended_daily_emails').show();
      }
      setSendingFrequency({
        output: '#mailpoet_recommended_daily_emails',
        only_daily: false,
        emails: emails,
        interval: interval
      });
      \$('#mta_frequency_emails').val(emails);
      \$('#mta_frequency_interval').val(interval);
    }

    function renderHostSendingFrequency() {
      var host = \$(this).find('option:selected').first();
      var frequencyType = \$(\"#mailpoet_sending_frequency\").find('option:selected').first().val();

      var emails =
        host.data('emails') || ";
        // line 1026
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["default_frequency"] ?? null), "website", array()), "emails", array()), "html", null, true);
        echo ";
      var interval =
        host.data('interval') || ";
        // line 1028
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["default_frequency"] ?? null), "website", array()), "interval", array()), "html", null, true);
        echo ";
      var fields =
        host.data('fields') || '';

      if (frequencyType === \"manual\") {
        return;
      } else {
        setSendingFrequency({
          output: '#mailpoet_recommended_daily_emails',
          only_daily: false,
          emails: emails,
          interval: interval
        });
      }

      \$('#mta_frequency_emails').val(emails);
      \$('#mta_frequency_interval').val(interval);
    }

    function updateSendingFrequency(method) {
      // get emails
      var options = {
        only_daily: true,
        emails: \$('#'+method+'_frequency_emails').val(),
        interval: \$('#'+method+'_frequency_interval').val()
      };

      var MINUTES_PER_DAY = 1440;
      var SECONDS_PER_DAY = 86400;

      options.daily_emails = ~~(
        (MINUTES_PER_DAY / options.interval) * options.emails
      );

      options.emails_per_second = (~~(
        ((options.daily_emails) / 86400) * 10)
      ) / 10;


      // format daily emails number according to locale
      options.daily_emails = options.daily_emails.toLocaleString();

      \$('#mailpoet_'+method+'_daily_emails').html(
        sending_frequency_template(options)
      );

      // update actual sending frequency values
      \$('#mta_frequency_emails').val(options.emails);
      \$('#mta_frequency_interval').val(options.interval);
    }

    function setSendingFrequency(options) {
      options.daily_emails = ~~((1440 / options.interval) * options.emails);

      // format daily emails number according to locale
      options.daily_emails = options.daily_emails.toLocaleString();
      \$(options.output).html(
        sending_frequency_template(options)
      );
    }

    Handlebars.registerHelper('format_time', function(value, block) {
      var label = null;
      var labels = {
        minute: \"";
        // line 1092
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("every minute");
        echo "\",
        minutes: \"";
        // line 1093
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("every %1\$d minutes");
        echo "\",
        hour: \"";
        // line 1094
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("every hour");
        echo "\",
        hours: \"";
        // line 1095
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("every %1\$d hours");
        echo "\"
      };

      // cast time as int
      value = ~~(value);

      // format time depending on the value
      if(value >= 60) {
        // we're dealing with hours
        if(value === 60) {
          label = labels.hour;
        } else {
          label = labels.hours;
        }
        value /= 60;
      } else {
        // we're dealing with minutes
        if(value === 1) {
          label = labels.minute;
        } else {
          label = labels.minutes;
        }
      }

      if(label !== null) {
        return label.replace('%1\$d', value);
      } else {
        return value;
      }
    });
  });

  // enable/disable MSS method activate button and notice
  function updateMSSActivationUI() {
    var \$ = jQuery;
    var group = \$('.mailpoet_sending_methods .mailpoet_active').data('group');
    var key_valid = !\$('.mailpoet_mss_key_valid').hasClass('mailpoet_hidden');

    if (group !== 'mailpoet') {
      \$('.mailpoet_actions .mailpoet_invalid_key').toggleClass('mailpoet_hidden', key_valid);
      \$('.mailpoet_actions .mailpoet_valid_key').toggleClass('mailpoet_hidden', !key_valid);
      \$('.mailpoet_other_sending_method_action').removeClass('button-primary').addClass('button-secondary').text('";
        // line 1136
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Configure");
        echo "');
    } else {
      \$('.mailpoet_actions .mailpoet_valid_key').addClass('mailpoet_hidden');
      \$('.mailpoet_activated').toggleClass('mailpoet_hidden', !key_valid);
      \$('.mailpoet_other_sending_method_action').removeClass('button-secondary').addClass('button-primary').text('";
        // line 1140
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Activate");
        echo "');
      \$('.mailpoet_invalid_key').toggleClass('mailpoet_hidden', key_valid);
      \$('.mailpoet_sending_methods li[data-group=\"'+group+'\"], .mailpoet_sending_methods li[data-group=\"'+group+'\"] > .mailpoet_status').toggleClass('mailpoet_invalid_key', !key_valid)
    }

    if (key_valid) {
        \$('.mailpoet_mss_activate_notice').toggle(group !== 'mailpoet');
      }
  };
</script>

";
        // line 1151
        echo $this->env->getExtension('MailPoet\Twig\Handlebars')->generatePartial($this->env, $context, "mailpoet_sending_frequency_template", "settings/templates/sending_frequency.hbs");
        // line 1154
        echo "
";
    }

    public function getTemplateName()
    {
        return "settings/mta.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1653 => 1154,  1651 => 1151,  1637 => 1140,  1630 => 1136,  1586 => 1095,  1582 => 1094,  1578 => 1093,  1574 => 1092,  1507 => 1028,  1502 => 1026,  1257 => 784,  1250 => 779,  1245 => 776,  1243 => 775,  1193 => 728,  1181 => 719,  1177 => 718,  1172 => 716,  1156 => 703,  1118 => 667,  1116 => 665,  1101 => 653,  1093 => 648,  1077 => 635,  1069 => 630,  1057 => 621,  1046 => 613,  1038 => 608,  1032 => 605,  1015 => 592,  1011 => 590,  1009 => 589,  997 => 581,  993 => 579,  991 => 578,  990 => 577,  978 => 567,  972 => 564,  967 => 561,  963 => 559,  961 => 557,  953 => 550,  949 => 548,  947 => 547,  942 => 544,  938 => 542,  936 => 541,  930 => 538,  922 => 533,  917 => 530,  913 => 528,  911 => 526,  902 => 519,  890 => 510,  885 => 507,  881 => 505,  879 => 503,  870 => 496,  858 => 487,  853 => 484,  849 => 482,  847 => 480,  837 => 472,  825 => 463,  820 => 460,  816 => 458,  814 => 456,  805 => 448,  802 => 447,  800 => 446,  787 => 436,  782 => 433,  778 => 431,  776 => 429,  767 => 421,  764 => 420,  762 => 419,  749 => 409,  744 => 406,  740 => 404,  738 => 402,  730 => 395,  721 => 392,  718 => 391,  714 => 389,  712 => 388,  708 => 387,  705 => 386,  701 => 385,  697 => 383,  694 => 382,  692 => 381,  682 => 374,  677 => 371,  673 => 369,  671 => 367,  661 => 359,  646 => 347,  641 => 344,  637 => 342,  635 => 340,  627 => 334,  615 => 325,  609 => 322,  604 => 319,  600 => 317,  598 => 315,  589 => 308,  582 => 303,  575 => 301,  569 => 299,  567 => 298,  563 => 297,  560 => 296,  556 => 294,  553 => 293,  549 => 291,  547 => 289,  546 => 288,  542 => 286,  539 => 285,  535 => 284,  530 => 282,  527 => 281,  521 => 279,  515 => 277,  513 => 276,  504 => 269,  500 => 267,  498 => 265,  489 => 258,  486 => 257,  482 => 255,  480 => 253,  474 => 249,  460 => 238,  450 => 230,  441 => 228,  437 => 226,  435 => 225,  431 => 224,  427 => 223,  423 => 222,  420 => 221,  416 => 220,  411 => 218,  392 => 202,  387 => 199,  383 => 197,  381 => 195,  374 => 189,  365 => 187,  361 => 185,  359 => 184,  355 => 183,  351 => 182,  347 => 181,  343 => 180,  340 => 179,  336 => 178,  332 => 177,  326 => 174,  323 => 173,  319 => 171,  317 => 169,  311 => 165,  298 => 155,  283 => 142,  277 => 140,  271 => 138,  269 => 137,  264 => 135,  256 => 129,  254 => 125,  250 => 124,  246 => 123,  242 => 122,  237 => 120,  232 => 118,  228 => 116,  224 => 115,  215 => 109,  209 => 108,  205 => 106,  203 => 102,  199 => 101,  195 => 100,  189 => 99,  183 => 96,  177 => 95,  171 => 94,  165 => 90,  163 => 88,  161 => 86,  159 => 85,  152 => 81,  148 => 80,  144 => 79,  140 => 78,  136 => 77,  129 => 73,  123 => 72,  117 => 69,  111 => 68,  104 => 64,  91 => 60,  81 => 53,  70 => 45,  61 => 39,  50 => 31,  40 => 24,  31 => 18,  23 => 12,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "settings/mta.html", "/home/proig/public_html/blog/wp-content/plugins/mailpoet/views/settings/mta.html");
    }
}
