<?php
require_once "header.php";

    $idProduto = $_GET['idproduto'];
?>
<div class="container mt-2">


<?php

$stmt2 = $db->prepare("
           SELECT * FROM matriz
           INNER JOIN proposta ON matriz.idMatriz = proposta.idMatriz
           INNER JOIN login ON login.id = proposta.id
           INNER JOIN markers ON markers.id = login.id
           WHERE matriz.idMatriz = '".$row['idMatriz']."' ORDER BY idProposta DESC");





$sqlProduto = $db->prepare(" SELECT * FROM produto
INNER JOIN login ON produto.id = login.id
where idproduto = '$idProduto'");
$sqlProduto->execute();
            if(is_array($sqlProduto) ? count($sqlProduto):true){
                foreach($sqlProduto as $rowProduto){

                    ?>
                    <nav aria-label="breadcrumb">
  <ol class="breadcrumb alert-light">
    <li class="breadcrumb-item"><a href="classificado.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo $rowProduto['titulo'];?></li>
  </ol>
</nav>
<div class="row">
<div class="col-md-4">



   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
  <?php
        $stmtIMG = $db->prepare("SELECT * FROM fotosproduto WHERE idproduto = '".$rowProduto['idproduto']."' LIMIT 1");
    $stmtIMG->execute();
    
        while($rowIMG = $stmtIMG->fetch()){
            ?>
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/uploads/<?php echo $rowIMG['nomefoto'] ?>" alt="<?php echo $rowProduto['titulo']?>">
    </div>
    <?php } ?>
<?php
    $stmtIMG = $db->prepare("SELECT * FROM fotosproduto WHERE idproduto = '".$rowProduto['idproduto']."' LIMIT 1,5");
    $stmtIMG->execute();
    
        while($rowIMG = $stmtIMG->fetch()){
            ?>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/uploads/<?php echo $rowIMG['nomefoto'] ?>" alt="<?php echo $rowProduto['titulo']?>">
    </div>
    <?php } ?>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Proximo</span>
  </a>
</div>

<div class="card">
  <div class="card-header">
    <strong>Dados do Vendedor</strong>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><i class="far fa-user mr-4"></i><?php echo $rowProduto['nome'];?></li>
    <li class="list-group-item"><i class="fas fa-city mr-4"></i><?php echo $rowProduto['cidade'];?> - <?php echo $rowProduto['estado'];?></li>
    <li class="list-group-item"><i class="fas fa-home mr-4"></i> Criatório <?php echo $rowProduto['criatorio'];?></li>
    <li class="list-group-item"><i class="fas fa-at mr-4"></i><?php echo $rowProduto['email'];?></li>
    <li class="list-group-item"><i class="fas fa-mobile-alt mr-4"></i><?php echo $rowProduto['whatsapp'];?></li>
  </ul>
</div>



</div>
<div class="col-md-8">
<div class="jumbotron">
  <h1 class="display-4"><?php echo $rowProduto['titulo'];?></h1>
  <p class="lead text-justify"><?php echo $rowProduto['descricao'];?></p>
  <hr class="my-4">
  <p>
  <?php
if($rowProduto['tipo'] == "Reprodutor" OR $rowProduto['tipo'] == "Matriz" OR $rowProduto['tipo'] == "Frango" OR $rowProduto['tipo'] == "Franga"){?>
<div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Medida</th>
      <th scope="col">Idade</th>
      <th scope="col">Preço</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowProduto['tipo']; ?></td>
      <td><?php echo $rowProduto['medida']; ?></td>
      <td><?php echo $rowProduto['idade']; ?></td>
      <td><?php echo $rowProduto['preco']; ?></td>
    </tr>
    <tr>
      <th scope="col">Rabo</th>
      <th scope="col">Asas</th>
      <th scope="col">Barbela</th>
      <th scope="col">Retirada</th>
    </tr>
    <tr>
      <td><?php echo $rowProduto['rabo']; ?></td>
      <td><?php echo $rowProduto['asas']; ?></td>
      <td><?php echo $rowProduto['barbela']; ?></td>
      <td><?php echo $rowProduto['retirada']; ?></td>
    </tr>
  </tbody>
</table>

<?php }else if($rowProduto['tipo'] == "Pintinho"){ ?>
    <div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Idade</th>
      <th scope="col">Preço</th>
      <th scope="col">Retirada</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowProduto['tipo']; ?></td>
      <td><?php echo $rowProduto['idade']; ?></td>
      <td><?php echo $rowProduto['preco']; ?></td>
      <td><?php echo $rowProduto['retirada']; ?></td>
    </tr>
    
  </tbody>
</table>
<!--fim table-->

<?php }else if($rowProduto['tipo'] == "Produto"){ ?>
    <div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Preço</th>
      <th scope="col">Retirada</th>
      <th scope="col">Quantidade</th>
      <th scope="col">Medida</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowProduto['tipo']; ?></td>
      <td><?php echo $rowProduto['preco']; ?></td>
      <td><?php echo $rowProduto['retirada']; ?></td>
      <td><?php echo $rowProduto['qtd']; ?></td>
      <td><?php echo $rowProduto['und']; ?></td>
    </tr>
    
  </tbody>
</table>
<!--fim table-->
<?php } ?>
  
  
  </p>
  <a class="btn btn-success btn-lg" href="https://api.whatsapp.com/send?phone=55<?php echo $rowProduto['whatsapp'] ?>&text=Olá,%20vi%20seu%20anúncio%20no%20*Pró IG*,%20tenho%20interesse%20no%20produto%20*<?php echo $rowProduto['titulo'];?>*%20-%20*R$%20<?php echo $rowProduto['preco']; ?>*" role="button"><i class="fab fa-whatsapp mr-4"></i> NEGOCIAR COM VENDEDOR</a>

</div>
</div>






</div><!--fim linha-->
<h2 class="mb-4"> </h2>
                    <?php }}?>

        <?php
   $stmtIMGGaleria = $db->prepare("SELECT * FROM fotosgaleria WHERE idGaleria = $idDaGaleria");
$stmtIMGGaleria->execute();

   while($rowIMGGaleria = $stmtIMGGaleria->fetch()){
       ?>
            
   <?php } ?>


</div>

   <!--FIM Produto-->


<?php
include "footer.php";
?>