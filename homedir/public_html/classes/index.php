<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Como enviar e-mail com o PHPMailer no PHP</title>
    </head>
    <body>
        <form action="mail.php">
            <input type="text" name="name" placeholder="Nome"/>
            <input type="email" name="email" placeholder="E-mail"/>
            <button type="submit" formmethod="POST">Enviar e-mail</button>
        </form>
    </body>
</html>
