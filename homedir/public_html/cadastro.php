<?php

ob_start();
session_start();
include "classes/config.php";
if(!isset($_SESSION['login'])){
    header('location: login.php');
}

$page = isset($_GET['p'])?$_GET['p']:'';
if($page=='add'){
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $cidade = $_POST['cidade'];
    $estado = $_POST['estado'];
    $criatorio = $_POST['criatorio'];
    $apelido = $_POST['apelido'];
    $dn = $_POST['dn'];
    $whatsapp = $_POST['whatsapp'];
    $senha = $_POST['senha'];
    $stmt = $db->prepare("INSERT INTO login values('',?,?,?,?,?,?,?,?,?)");
    $stmt->bindParam(1,$nome);
    $stmt->bindParam(2,$email);
    $stmt->bindParam(3,$cidade);
    $stmt->bindParam(4,$estado);
    $stmt->bindParam(5,$criatorio);
    $stmt->bindParam(6,$apelido);
    $stmt->bindParam(7,$dn);
    $stmt->bindParam(8,$whatsapp);
    $stmt->bindParam(9,$senha);
    if($stmt->execute()){
        echo "USuário adicionado com sucesso!";
    }else{
        echo "Falha ao adicionar usuario";
    }
    
}else if($page=='editar'){
    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $cidade = $_POST['cidade'];
    $estado = $_POST['estado'];
    $criatorio = $_POST['criatorio'];
    $apelido = $_POST['apelido'];
    $dn = $_POST['dn'];
    $whatsapp = $_POST['whatsapp'];
    $senha = $_POST['senha'];
    $stmt = $db->prepare("UPDATE login SET nome=?, email=?, cidade=?, estado=?, criatorio=?, apelido=?, dn=?, whatsapp=?, senha=? WHERE id=?");
    $stmt->bindParam(1,$nome);
    $stmt->bindParam(2,$email);
    $stmt->bindParam(3,$cidade);
    $stmt->bindParam(4,$estado);
    $stmt->bindParam(5,$criatorio);
    $stmt->bindParam(6,$apelido);
    $stmt->bindParam(7,$dn);
    $stmt->bindParam(8,$whatsapp);
    $stmt->bindParam(9,$senha);
    $stmt->bindParam(10,$id);
    if($stmt->execute()){
        echo "Usuário atualizado com sucesso!";
    }else{
        echo "Falha ao atualizar usuário";
    }
    
}else if($page=='deletar'){
    $id = $_GET['id'];
    $stmt = $db->prepare("DELETE FROM login WHERE id=?");
    $stmt->bindParam(1, $id);
    if($stmt->execute()){
        echo "Usuário excluido com sucesso!";
    }else{
        echo "Falha ao excluir usuário";
    }
    
}else{
    //WHERE id = '".$_SESSION['id']."'
    $stmt = $db->prepare("SELECT * FROM login WHERE id = '".$_SESSION['id']."' ORDER BY nome ASC  ");
    $stmt->execute();
    while($row = $stmt->fetch()){
        ?>
        <tr>

            <td><?php echo $row['nome']?></td>
            <td><?php echo $row['email']?></td>
            <td><?php echo $row['login']?></td>

            <td>
                <button class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?php echo $row['id'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDITAR</button>
                
                
<div class="modal fade" id="editModal-<?php echo $row['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['id'] ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editLabel-<?php echo $row['id']?>">ATUALIZAR CATEGORIA</h4>
      </div>
      <form>
      <div class="modal-body">
      <input type="hidden" id="<?php echo $row['id']?>" value="<?php echo $row['id'] ?>">
      <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" id="nome-<?php echo $row['id']?>" value="<?php echo $row['nome']?>">
                    </div>
                    <div class="form-group">
                        <label for="login">Login de Acesso:</label>
                        <input type="text" class="form-control" id="login-<?php echo $row['id']?>" value="<?php echo $row['login']?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email-<?php echo $row['id']?>" value="<?php echo $row['email']?>">
                    </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
        <button type="submit" onclick="atualizarLogin(<?php echo $row['id'] ?>)" class="btn btn-primary">ATUALIZAR</button>
      </div>
      </form>
    </div>
  </div>
</div>

                <button class="btn btn-danger" onclick="deletarLogin(<?php echo $row['id']?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> EXCLUIR</button>
                
                
                
<!--Formulario de Upload de Fotos-->
            <!-- Modal -->

            <?php
    if(isset($_FILES['fotos'])){
        foreach ($_FILES['fotos']['name'] as $file =>$nomeFoto){
            $filename = date('Ymd-His', time()).mt_rand().'-'.$nomeFoto;
            try{
                if(move_uploaded_file($_FILES['fotos']['tmp_name'][$file], 'img/perfil/'.$filename)){
                    $stmtD = $db->prepare("DELETE FROM fotoperfil WHERE id = '".$_SESSION['id']."'");
                    $stmtD->execute();
                    

                    $stmtP = $db->prepare("UPDATE login SET pontos = pontos + 1 WHERE id = '".$_SESSION['id']."'");
                    $stmtP->execute();
                    
                    

                    
                    $stmt = $db->prepare("insert into fotoperfil (nomeFoto, id) values(:nomeFoto, :id)");
                    $id = (int)$_POST['id'];
					$stmt->bindParam(':nomeFoto', $filename, PDO::PARAM_STR);   
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);   					
                    $stmt->execute();
                    echo '<script>location.href ="cadDados.php"; </script>';
                }
            }catch (PDOException $e) {
                echo "DataBase Error: The user could not be added.<br>".$e->getMessage();
                exit;
            } catch (Exception $e) {
                echo "General Error: The user could not be added.<br>".$e->getMessage();
                exit;
        }
    }
    }
?>
               

                <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="fotolLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="fotoLabel">ENVIAR FOTOS</h4>
                            </div>
                            <form method="post" action='loginCadastro.php' enctype="multipart/form-data">
                                <div class="modal-body">
                                   
                                   
<div class="form-group">
                        <label for="id">Foto</label>
                    <select class="form-control" name="id" id="id">
                       <?php                          
                    $stmt = $db->prepare("SELECT * FROM login WHERE id = '".$_SESSION['id']."'");
                        $stmt->execute();
                        while($row = $stmt->fetch()){
                            ?>
                            <option value="<?php echo $row['id']?>">
                                <?php echo $row['login']; ?>
                            </option>
                        <?php } ?>
                    </select>
                    </div>  

                                    <div class="form-group">
                                        <input type="file" name="fotos[]" multiple>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" class="btn btn-primary">UPLOAD</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
<button class="btn btn-default pegaId" data-toggle="modal" data-target="#fotoModal" id="<?php echo $row['id']?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> FOTO</button>
                <!--Fim Formulario de Upload de Fotos-->
   
                
                
                
                
            </td>
        </tr>
        <?php
    }
}
?>
