<?php 
require_once "header.php";
?>

<div class="container">
<div class="row mt-3">
    <div class="col-md-3 mt-4">
    <form action="" method="post">
                <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <i class="fas fa-search"></i> BUSCA AVANÇADA
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse toglle" aria-labelledby="headingOne" aria-expanded="false" data-parent="#accordionExample">
      <div class="card-body">        
      <ul class="payment-methods">
          <h5 class="card-title">Tipo:</h5>
                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Reprodutor" value="Reprodutor">
                                <i class="fas fa-check"></i> <label for="Reprodutor">Reprodutor</label>
                            </li>

                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Matriz" value="Matriz">
                                <i class="fas fa-check"></i> <label for="Matriz">Matriz</label>
                            </li>
                            
                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Frango" value="frango">
                                <i class="fas fa-check"></i> <label for="Frango">Frango</label>
                            </li>
                            
                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Franga" value="Franga">
                                <i class="fas fa-check"></i> <label for="Franga">Franga</label>
                            </li>
                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Pintinho" value="Pintinho">
                                <i class="fas fa-check"></i> <label for="Pintinho">Pintinho</label>
                            </li>
                            <li class="payment-method">
                                <input name="payment_methods" type="radio" id="Produto" value="Produto">
                                <i class="fas fa-check"></i> <label for="Produto">Produto</label>
                            </li>

                        </ul>
                        <br>
                        <ul class="payment-methods">
                        <h5 class="card-title">Asas:</h5>
                            <li class="payment-method">
                                <input name="payment_methodsA" type="radio" id="Encaixada" value="Encaixada">
                                <i class="fas fa-check"></i> <label for="Encaixada">Encaixada</label>
                            </li>

                            <li class="payment-method">
                                <input name="payment_methodsA" type="radio" id="Desencaixada" value="Desencaixada">
                                <i class="fas fa-check"></i> <label for="Desencaixada">Desencaixada</label>
                            </li>

                        </ul>
                        <br>
                        <ul class="payment-methods">
                        <h5 class="card-title">Barbela:</h5>
                            <li class="payment-method">
                                <input name="payment_methodsB" type="radio" id="Ausente" value="Ausente">
                                <i class="fas fa-check"></i> <label for="Ausente">Ausente</label>
                            </li>

                            <li class="payment-method">
                                <input name="payment_methodsB" type="radio" id="De Boi" value="De Boi">
                                <i class="fas fa-check"></i> <label for="De Boi">De Boi</label>
                            </li>
                            <li class="payment-method">
                                <input name="payment_methodsB" type="radio" id="De Boi (com dupla)" value="De Boi (com dupla)">
                                <i class="fas fa-check"></i> <label for="De Boi (com dupla)">De Boi (com dupla)</label>
                            </li>

                        </ul>
                        <br>
                        <input type="submit" name="enviar" class="btn btn-success mt-3">
                </form>
      </div>
      
    </div>
    
  </div>

</div>
                    
    </div><!--FIM DA BUSCA-->


    <div class="col-md-9 mt-4">
    <div class="row">

<?php

            if (isset($_POST['enviar'])) {
    $tipo = $_POST['payment_methods'];
    $asas = $_POST['payment_methodsA'];
    $barbela = $_POST['payment_methodsB'];
           
    
     $stmtM = $db->prepare("SELECT * FROM produto WHERE tipo like :tipo OR asas like :asas OR barbela like :barbela ORDER BY titulo DESC");
     $stmtM->bindParam( ':tipo', $tipo );
     $stmtM->bindParam( ':asas', $asas );
     $stmtM->bindParam( ':barbela', $barbela );
    $stmtM->execute();
    $total = $stmtM->rowCount();
if($total == null){
    ?>



<div class="alert alert-warning alert-dismissible fade show" role="alert" style="width:99%">
  <strong>Desculpe!</strong> Nenhum produto com sua busca foi encontrado, tente uma nova busca
  <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="window.location.href='classificado.php';">
    <span aria-hidden="true">&times;</span>
  </button>
</div>


    <?php
}else if($total >= 1){
    while($rowR = $stmtM->fetch()){
                ?>
                        <div class="col-12 col-md-6">
                            <div class="card mb-3">
                            <div class="card-header text-center">
                            <h5 class="card-title"><?php echo $rowR['titulo']?></h5>
  </div>
                               <?php
        $stmtIMG = $db->prepare("SELECT * FROM fotosproduto WHERE idproduto = '".$rowR['idproduto']."' LIMIT 1");
    $stmtIMG->execute();
    
        while($rowIMG = $stmtIMG->fetch()){
            ?>
          <div class="estiloImg">
          <img class="card-img-top" src="img/uploads/<?php echo $rowIMG['nomefoto'] ?>" alt="<?php echo $rowR['titulo']?>">
                                </div>
   <?php } ?>
                                
                                <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5"><?php echo $rowR['tipo']?></div>
                                    <div class="col-md-7 text-right"><small>R$</small> <strong><?php echo $rowR['preco']?></strong></div>
                                </div>   
                                  
                                  <div class="row mt-2">
                                    <div class="col-md-5">
                                    <?php
if($rowR['tipo'] == "Reprodutor" OR $rowR['tipo'] == "Matriz" OR $rowR['tipo'] == "Frango" OR $rowR['tipo'] == "Franga"){
    echo '<small>Tamanho:</small><br>';
    echo $rowR['medida']." cm";
}else if($rowR['tipo'] == "Pintinho"){
    echo '<small>Idade:</small><br>';
    echo $rowR['idade']." dias";
}else{
    echo '<small>Retirada:</small><br>';
    echo $rowR['retirada'];
}
    ?>
                                    </div>
                                    <div class="col-md-7 text-right">
                                    
                                    <?php
if($rowR['tipo'] == "Reprodutor" OR $rowR['tipo'] == "Matriz" OR $rowR['tipo'] == "Frango" OR $rowR['tipo'] == "Franga"){
    echo '<small>Crista:</small><br>';
    echo $rowR['crista'];
}else if($rowR['tipo'] == "Pintinho"){
    echo '<small>Idade:</small><br>';
    echo $rowR['idade']." dias";
}else{
    echo '<small>Retirada:</small><br>';
    echo $rowR['retirada'];
}
    ?>
                                    
                                    </div>
                                </div>
  
                                   
                                   
                                   
                                   
                                    
                                    <a href="detalheproduto.php?idproduto=<?php echo $rowR['idproduto']?>" class="btn btn-primary mt-3">+ DETALHES</a>
                                </div>
                            </div>
                        </div>
                        <?php }}?>
                    </div>
                </div>
               
               
               
                <?php }else{ ?>
                <?php
 $stmtR = $db->prepare("SELECT * FROM produto ORDER BY titulo DESC");
    $stmtR->execute();
        
    while($rowR = $stmtR->fetch()){   ?>
                <div class="col-12 col-md-6">
                            <div class="card mb-3">
                            <div class="card-header text-center">
                            <h5 class="card-title"><?php echo $rowR['titulo']?></h5>
  </div>
                               <?php
        $stmtIMG = $db->prepare("SELECT * FROM fotosproduto WHERE idproduto = '".$rowR['idproduto']."' LIMIT 1");
    $stmtIMG->execute();
    
        while($rowIMG = $stmtIMG->fetch()){
            ?>
          <div class="estiloImg">
          <img class="card-img-top" src="img/uploads/<?php echo $rowIMG['nomefoto'] ?>" alt="<?php echo $rowR['titulo']?>">
                                </div>
   <?php } ?>
                                
                                <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5">
                                    <small>Tipo:</small><br>
                                    <?php echo $rowR['tipo']?></div>
                                    <div class="col-md-7 text-right">
                                    <small>Preço:</small><br>
                                    <small>R$</small> <strong><?php echo $rowR['preco']?></strong></div>
                                </div>   
                                  
                                  <div class="row mt-2">
                                    <div class="col-md-5">
                                    <?php
if($rowR['tipo'] == "Reprodutor" OR $rowR['tipo'] == "Matriz" OR $rowR['tipo'] == "Frango" OR $rowR['tipo'] == "Franga"){
    echo '<small>Tamanho:</small><br>';
    echo $rowR['medida']." cm";
}else if($rowR['tipo'] == "Pintinho"){
    echo '<small>Retirada:</small><br>';
    echo $rowR['retirada'];
}else{
    echo '<small>Retirada:</small><br>';
    echo $rowR['retirada'];
}
    ?>
                                    </div>
                                    <div class="col-md-7 text-right">
                                    <?php
if($rowR['tipo'] == "Reprodutor" OR $rowR['tipo'] == "Matriz" OR $rowR['tipo'] == "Frango" OR $rowR['tipo'] == "Franga"){
    echo '<small>Crista:</small><br>';
    echo $rowR['crista'];
}else if($rowR['tipo'] == "Pintinho"){
    echo '<small>Idade:</small><br>';
    echo $rowR['idade']." dias";
}else{
    echo '<small>Retirada:</small><br>';
    echo $rowR['retirada'];
}
    ?>
                                    </div>
                                </div>
  
                                   
                                   
                                   
                                   
                                    
                                    <a href="detalheproduto.php?idproduto=<?php echo $rowR['idproduto']?>" class="btn btn-primary mt-3">+ DETALHES</a>
                                </div>
                            </div>
                        </div>
                <?php }}?>


    </div>
</div>







<?php 
require_once "footer.php";
?>