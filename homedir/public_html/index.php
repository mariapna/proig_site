<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>ProIG - </title>
<link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<body>
<header>
<div class="logo">
    <img src="img/logo_proig.png" width="100">
    <br>
    <h2>O PORTAL DO ÍNDIO GIGANTE</h2>

</div>
</header>
<main>

        
<section>

<div class="sessao">
<i class="fas fa-info-circle fa-2x"></i>
<a href="">INFORMAÇÕES</a><br>
<small>Entrevistas, vídeos, manejo sobre o Índio Gigante</small>
</div>

<div class="sessao">
<i class="fas fa-shopping-cart fa-2x"></i>
<a href="classificado.php">COMPRE</a><br>
<small>Maior classificados de Índio Gigante</small>
</div>

<div class="sessao">
<i class="fas fa-dollar-sign fa-2x"></i>
<a href="login.php">VENDA</a><br>
<small>Facilidade nas vendas do seu criatório</small>
</div>

<div class="sessao">
<i class="far fa-calendar-alt fa-2x"></i>
<a href="">EVENTOS</a><br>
<small>Fique por dos grandes encontros!</small>
</div>
</section>
</main>

</body>
</html>


