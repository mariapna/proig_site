<?php
include "config.php";
session_start();
if(!isset($_SESSION['username'])){
    header('location: login.php');
}
$page = isset($_GET['p'])?$_GET['p']:'';
if($page=='add'){
    $texto = $_POST['texto'];
    $stmt = $db->prepare("INSERT INTO transporte values('',?)");
    $stmt->bindParam(1,$texto);
    if($stmt->execute()){
        echo "Bilhete Eletronico cadastrado com sucesso!";
    }else{
        echo "Falha ao adicionar Bilhete Eletronico";
    }
    
}else if($page=='editar'){
    $idTransporte = $_POST['idTransporte'];
    $texto = $_POST['texto'];
    $stmt = $db->prepare("UPDATE transporte SET texto=? WHERE idTransporte=?");
    $stmt->bindParam(1,$texto);
    $stmt->bindParam(2,$idTransporte);
    if($stmt->execute()){
        echo "Bilhete Eletronico atualizado com sucesso!";
    }else{
        echo "Falha ao atualizar Bilhete Eletronico";
    }
    
}else if($page=='deletar'){
    $idTransporte = $_GET['idTransporte'];
    $stmt = $db->prepare("DELETE FROM transporte WHERE idTransporte=?");
    $stmt->bindParam(1, $idTransporte);
    if($stmt->execute()){
        echo "Bilhete Eletronico excluido com sucesso!";
    }else{
        echo "Falha ao excluir Bilhete Eletronico";
    }
    
}else{
    $stmt = $db->prepare("SELECT * FROM transporte");
    $stmt->execute();
    
    foreach($stmt as $row){
        ?>
    <tr>

        <td>

            <?php
           $stmt2 = $db->prepare("SELECT p.*, f.nome_foto FROM transporte p INNER JOIN fotostransporte f ON p.idTransporte = f.idTransporte WHERE p.idTransporte = '".$row['idTransporte']."'");
       $stmt2->execute();
        foreach($stmt2 as $rows){

        ?>
                <img src="../SITE/cartoes/<?php echo $rows['nome_foto']; ?>" width="40"/>

                <?php } ?>
        </td>



        <td>
            <?php echo $row['texto']?>
        </td>
        <td>


<button class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?php echo $row['idTransporte'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>


                <div class="modal fade" id="editModal-<?php echo $row['idTransporte'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['idTransporte'] ?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="editLabel-<?php echo $row['idTransporte']?>">Atualizar Texto</h4>
                            </div>
                            <form>
                                <div class="modal-body">
                                    <input type="hidden" id="<?php echo $row['idTransporte']?>" value="<?php echo $row['idTransporte'] ?>">
                                       
                                      <div class="form-group">
                        <label for="texto">Texto</label>
                        <textarea class="form-control" rows="3" id="texto-<?php echo $row['idTransporte']?>"><?php echo $row['texto'] ?></textarea>
                    </div>               
                                       

                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" onclick="atualizarTransporte(<?php echo $row['idTransporte'] ?>)" class="btn btn-primary">Atualizar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <button class="btn btn-danger" onclick="deletarTransporte(<?php echo $row['idTransporte']?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>

            <!--Formulario de Upload de Fotos-->
            <!-- Modal -->

            <?php
    if(isset($_FILES['fotos'])){
        $titulo = $_POST['titulo'];
        $descricao = $_POST['descricao'];
        foreach ($_FILES['fotos']['name'] as $file =>$nome_foto){
            $filename = date('Ymd-His', time()).mt_rand().'-'.$nome_foto;
            try{
                if(move_uploaded_file($_FILES['fotos']['tmp_name'][$file], '../SITE/cartoes/'.$filename)){
                    $stmt = $db->prepare("insert into fotostransporte (nome_foto, titulo, descricao, idTransporte) values(:nome_foto, :titulo, :descricao, :idTransporte)");
                    $idTransporte = (int)$_POST['idTransporte'];
					$stmt->bindParam(':nome_foto', $filename, PDO::PARAM_STR); 
                    $stmt->bindParam(':titulo', $titulo, PDO::PARAM_STR); 
                    $stmt->bindParam(':descricao', $descricao, PDO::PARAM_STR); 
					$stmt->bindParam(':idTransporte', $idTransporte, PDO::PARAM_INT);   					
                    $stmt->execute();
                    echo '<script>location.href ="cadBilheteEletronico.php"; </script>';
                }
            }catch (PDOException $e) {
                echo "DataBase Error: The user could not be added.<br>".$e->getMessage();
                exit;
            } catch (Exception $e) {
                echo "General Error: The user could not be added.<br>".$e->getMessage();
                exit;
        }
    }
    }
?>
               

                <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="fotolLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="fotoLabel">Enviar Fotos</h4>
                            </div>
                            <form method="post" action='bilhete.php' enctype="multipart/form-data">
                                <div class="modal-body">
                                   
                                   
<div class="form-group">
                        <label for="idTransporte">Titulo</label>
                    <select class="form-control" name="idTransporte" id="idTransporte">
                       <?php                          
                    $stmt = $db->prepare("SELECT * FROM transporte ORDER BY idTransporte ASC");
                        $stmt->execute();
                        while($row = $stmt->fetch()){
                            ?>
                            <option value="<?php echo $row['idTransporte']?>">
                                <?php echo $row['idTransporte']; ?>
                            </option>
                        <?php } ?>
                    </select>
                    </div>  
                                    <div class="form-group">
                                        <input type="file" name="fotos[]" multiple>
                                    </div>
                                <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="digite o titulo">
                    </div>
<div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" rows="6" name="descricao" id="descricao"></textarea>
                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" class="btn btn-primary">UPLOAD</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
<button class="btn btn-default pegaId" data-toggle="modal" data-target="#fotoModal" id="<?php echo $row['idTransporte']?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></button>
                <!--Fim Formulario de Upload de Fotos-->

  

                
        </td>
    </tr>

    <?php
    }
}
?>