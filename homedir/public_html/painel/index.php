<?php include"header.php";?>

    <div class="row">
     <div class="col-md-12">

        <ol class="breadcrumb">
         <li class="active"><i class="fa fa-dashboard"></i> <small>Olá</small> <?php echo $_SESSION['nome'];?></li>
          <li class="pull-right">
             <?php
             date_default_timezone_set('America/Sao_Paulo');
             $date = date('d/m/Y H:i');
             echo $date;
             ?>
          </li>
        </ol>
      </div>
     </div> 
                                                 
        <div class="row" >
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="far fa-user fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    Seus Dados
                                </div>
                                <div class="circle-tile-number text-faded">
                                    01
                                    <span id="sparklineA"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">EDITAR</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                <i class="fas fa-dollar-sign fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    Vender
                                </div>
                                <div class="circle-tile-number text-faded">
                                    01
                                </div>
                                <a href="cadproduto.php" class="circle-tile-footer">VENDER</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-bell fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    ALERTAS
                                </div>
                                <div class="circle-tile-number text-faded">
                                    1 novo
                                </div>
                                <a href="#" class="circle-tile-footer">VER ALERTAS</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-tasks fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    RELATÓRIOS
                                </div>
                                <div class="circle-tile-number text-faded">
                                    10
                                    <span id="sparklineB"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">VER RELATORIOS</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-shopping-cart fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    EFETIVAR COMPRA
                                </div>
                                <div class="circle-tile-number text-faded">
                                    02
                                    <span id="sparklineC"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">EFETIVAR COMPRA</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-comments fa-fw fa-2x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    MENSAGENS
                                </div>
                                <div class="circle-tile-number text-faded">
                                    02 novas
                                    <span id="sparklineD"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">VER MENSAGENS</a>
                            </div>
                        </div>
                    </div>
                </div>
                                                     
        
 
   
<?php include"footer.php";?>
