<?php include"header.php";?>
<button class="btn btn-primary" data-toggle="modal" data-target="#addEmpresa"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> CADASTRAR EMPRESA</button><br>
<hr><br>

<div class="modal fade" id="addEmpresa" tabindex="-1" role="dialog" aria-labelledby="addLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLabel">Inserir Empresa</h4>
            </div>
            <form>
                <div class="modal-body">


                    <div class="form-group">
                        <label for="empresa">Empresa</label>
                        <textarea class="form-control" id="empresa"></textarea>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                    <button type="submit" onclick="salvarEmpresa()" class="btn btn-primary">SALVAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>EMPRESA</th>
            <th width="110">AÇÃO</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<div class="row">

</div>
<script>
    function salvarEmpresa() {
        var empresa = $('#empresa').val();
        $.ajax({
            type: "POST",
            url: "empresa.php?p=add",
            data: "empresa="+empresa,
            success: function(msg) {
                alert('empresa cadastrada com sucesso!');
            }

        });
    }

    function verDados() {
        $.ajax({
            type: "GET",
            url: "empresa.php",
            success: function(data) {
                $('tbody').html(data);
            }
        })
    }

    function atualizarEmpresa(str) {
        var idEmpresa = str;
        var empresa = $('#empresa-' + str).val();
        $.ajax({
            type: "POST",
            url: "empresa.php?p=editar",
            data: "empresa=" + empresa + "&idEmpresa=" + idEmpresa,
            success: function(data) {
                verDados();
            }
        });

    }

    function deletarEmpresa(str) {
        var idEmpresa = str;
        $.ajax({
            type: "GET",
            url: "empresa.php?p=deletar",
            data: "idEmpresa=" + idEmpresa,
            success: function(data) {
                verDados();
            }
        })
    }
</script>
<?php include"footer.php";?>