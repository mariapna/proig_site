<?php include"header.php"; ?>

<div class="row">
    <div class="col-md-4">
    <button class="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#addCateg"><b>VENDER AVES</b></button>
    </div>

    <div class="col-md-4">
    <button class="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#addCategDois"><b>VENDER PINTINHOS</b></button>
        </div>

        <div class="col-md-4">
        <button class="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#addCategTres"><b>VENDER PRODUTOS</b></button>
        </div>
</div>

<br>

<?php if(isset($_GET['ok'])): ?>
	<h1>Inserido com sucesso</h1>
<?php endif; ?>

<!--inicio modal 1-->
<div class="modal fade" id="addCateg" tabindex="-1" role="dialog" aria-labelledby="addLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLabel">Vender Ave</h4>
            </div>
            <form enctype="multipart/form-data" data-toggle="validator">
                <div class="modal-body">

<div class="row">
    <div class="col-md-6">
    <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="titulo" placeholder="digite o titulo" required>
                    </div>
    </div>
    
    <div class="col-md-6">
    <div class="form-group">
                        <label for="medida">Medida (em centimetros)</label>
                        <input  type="text" class="form-control" id="medida" placeholder="digite o medida da ave">
                    </div>
    </div>
</div>
                    

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="tipo">Tipo</label><br>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipo" value="Reprodutor"> Reprodutor
            </label>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipo" value="Matriz"> Matriz
            </label>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipo" value="Frango"> Frango
            </label>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipo" value="Franga"> Franga
            </label>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="crista">Crista</label><br>
            <label class="radio-inline">
                <input type="radio" name="crista" id="crista" value="Bola"> Bola
            </label>
            <label class="radio-inline">
                <input type="radio" name="crista" id="crista" value="Ervilha"> Ervilha
            </label>

        </div>
    </div>

</div>   
   
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="idade">Idade da Ave (em meses)</label>
            <input type="text" class="form-control" id="idade" placeholder="digite a idade da ave">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="preco">Preço da Ave</label>
            <input type="text" class="form-control" id="preco" placeholder="digite o preço da ave" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
        </div>
    </div>



</div>

<div class="row">
    <div class="col-md-6"><div class="form-group">
                                    <label for="asas">Asas</label><br>
                                <label class="radio-inline">
            <input type="radio" name="asas" id="asas" value="Encaixada"> Encaixada
            </label>
            <label class="radio-inline">
            <input type="radio" name="asas" id="asas" value="Desencaixada"> Desencaixada
            </label>
            </div>
            </div>

            <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="barbela">Barbela</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela" value="Ausente"> Ausente
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela" value="De Boi"> De Boi
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela" value="De Boi (com dupla)"> De Boi (com dupla)
                                                </label>
                                            </div>
                                        </div>

    
</div>

               
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="rabo">Rabo</label><br>
            <label class="radio-inline">
                <input type="radio" name="rabo" id="rabo" value="Baixo"> Baixo
            </label>
            <label class="radio-inline">
                <input type="radio" name="rabo" id="rabo" value="Medio Baixo"> Medio Baixo
            </label>
            <label class="radio-inline">
                <input type="radio" name="rabo" id="rabo" value="Medio"> Medio
            </label>
            <label class="radio-inline">
                <input type="radio" name="rabo" id="rabo" value="Medio Alto"> Medio Alto
            </label>
            <label class="radio-inline">
                <input type="radio" name="rabo" id="rabo" value="Alto"> Alto
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="retirada">Retirada</label><br>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retirada" value="No Local"> No Local
            </label>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retirada" value="à Combinar"> à Combinar
            </label>
        </div>
    </div>
</div>


                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" rows="6" id="descricao"></textarea>
                    </div>
                    
                    <input type="hidden" id="id" value="<?php echo $_SESSION['id']?>" >
      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                    <button type="submit" onclick="salvarProduto()" class="btn btn-primary">VENDER</button>
                </div>
            </form>
        </div>
    </div>
</div> <!--fim modal 1-->


<!--inicio modal 2-->
<div class="modal fade" id="addCategDois" tabindex="-1" role="dialog" aria-labelledby="addLabelDois">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLabelDois">Cadastrar Pintinho</h4>
            </div>
            <form enctype="multipart/form-data">
                <div class="modal-body">

<div class="row">
    <div class="col-md-12">
    <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="tituloDois" placeholder="digite o titulo">
                    </div>
    </div>

    

</div>
                    

<div class="row">
    
<div class="col-md-6">
        <div class="form-group">
            <label for="preco">Preço da Ave</label>
            <input type="text" class="form-control" id="precoDois" placeholder="digite o preço da ave" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <label for="idade">Idade da Ave (em dias)</label>
            <input type="text" class="form-control" id="idadeDois" placeholder="digite a idade do pintinho">
        </div>
    </div>
</div>   

               
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label for="retirada">Retirada</label><br>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retiradaDois" value="No Local"> No Local
            </label>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retiradaDois" value="à Combinar"> à Combinar
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="tipo">Tipo</label><br>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipoDois" value="Pintinho" checked> Pintinho
            </label>
        </div>
    </div>
</div>


                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" rows="6" id="descricaoDois"></textarea>
                    </div>
                    
                    <input type="hidden" id="id" value="<?php echo $_SESSION['id']?>" >
      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                    <button type="submit" onclick="salvarProdutoDois()" class="btn btn-primary">VENDER</button>
                </div>
            </form>
        </div>
    </div>
</div> <!--fim modal 2-->

<!--inicio modal 3-->
<div class="modal fade" id="addCategTres" tabindex="-1" role="dialog" aria-labelledby="addLabelTres">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLabelTres">Cadastrar Produto</h4>
            </div>
            <form enctype="multipart/form-data">
                <div class="modal-body">

<div class="row">
    <div class="col-md-12">
    <div class="form-group">
                        <label for="titulo">Produto</label>
                        <input type="text" class="form-control" id="tituloTres" placeholder="digite o nome do Produto">
                    </div>
    </div>

    

</div>
                    

<div class="row">
    
<div class="col-md-4">
        <div class="form-group">
            <label for="preco">Preço do Produto</label>
            <input type="text" class="form-control" id="precoTres" placeholder="digite o preço do produto" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="retirada">Retirada</label><br>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retiradaTres" value="No Local"> No Local
            </label>
            <label class="radio-inline">
                <input type="radio" name="retirada" id="retiradaTres" value="à Combinar"> à Combinar
            </label>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="tipo">Tipo</label><br>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="tipoTres" value="Produto" checked> Produto
            </label>
        </div>
        
    </div>

</div>   


<div class="row">
    
<div class="col-md-6">
        <div class="form-group">
            <label for="qtd">Quantidade</label>
            <input type="text" class="form-control" id="qtdTres" placeholder="Quantidade do produto">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="und">Medida</label><br>
            <label class="radio-inline">
                <input type="radio" name="und" id="undTres" value="UND" checked> UND
            </label>
            <label class="radio-inline">
                <input type="radio" name="und" id="undTres" value="DZ"> DZ
            </label>
            <label class="radio-inline">
                <input type="radio" name="und" id="undTres" value="KG"> KG
            </label>
            <label class="radio-inline">
                <input type="radio" name="und" id="undTres" value="M"> M
            </label>
            <label class="radio-inline">
                <input type="radio" name="und" id="undTres" value="L"> L
            </label>
        </div>
    </div>

    

</div> 


                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" rows="6" id="descricaoTres"></textarea>
                    </div>
                    
                    <input type="hidden" id="id" value="<?php echo $_SESSION['id']?>" >
      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                    <button type="submit" onclick="salvarProdutoTres()" class="btn btn-primary">VENDER</button>
                </div>
            </form>
        </div>
    </div>
</div> <!--fim modal 3-->



<div class="row recebeDados"></div>

<script>
    function salvarProduto(){
        var titulo = $('#titulo').val();
        var medida = $('#medida').val();
        var tipo = $('#tipo:checked').val();
        var crista = $('#crista:checked').val();
        var idade = $('#idade').val();
        var preco = $('#preco').val();
        var asas = $('#asas:checked').val();
        var barbela = $('#barbela:checked').val();
        var rabo = $('#rabo:checked').val();
        var retirada = $('#retirada:checked').val();
        var descricao = $('#descricao').val();
        var id = $('#id').val(); 
        $.ajax({
            type: "POST",
            url: "produto.php?p=add",
            data: "titulo="+titulo+"&medida="+medida+"&tipo="+tipo+"&crista="+crista+"&idade="+idade+"&preco="+preco+"&asas="+asas+"&barbela="+barbela+"&rabo="+rabo+"&retirada="+retirada+"&descricao="+descricao+"&id="+id,
            success: function(msg){
                alert('Ave cadastrada com sucesso!');
            }

        });
    }

    function salvarProdutoDois(){
        var titulo = $('#tituloDois').val();
        var tipo = $('#tipoDois:checked').val();
        var idade = $('#idadeDois').val();
        var preco = $('#precoDois').val();
        var retirada = $('#retiradaDois:checked').val();
        var descricao = $('#descricaoDois').val(); 
        var id = $('#id').val(); 
        $.ajax({
            type: "POST",
            url: "produto.php?p=addpintinho",
            data: "titulo="+titulo+"&tipo="+tipo+"&idade="+idade+"&preco="+preco+"&retirada="+retirada+"&descricao="+descricao+"&id="+id,
            success: function(msg){
                alert('Pintinho cadastrado com sucesso!');
            }

        });
    }

    function salvarProdutoTres(){
        var titulo = $('#tituloTres').val();
        var tipo = $('#tipoTres:checked').val();
        var preco = $('#precoTres').val();
        var retirada = $('#retiradaTres:checked').val();
        var descricao = $('#descricaoTres').val();
        var id = $('#id').val(); 
        var qtd = $('#qtdTres').val();
        var und = $('#undTres').val();
        $.ajax({
            type: "POST",
            url: "produto.php?p=addproduto",
            data: "titulo="+titulo+"&tipo="+tipo+"&preco="+preco+"&retirada="+retirada+"&descricao="+descricao+"&id="+id+"&qtd="+qtd+"&und="+und,
            success: function(msg){
                alert('Produto cadastrado com sucesso!');
            }

        });
    }

function verDados(){
    $.ajax({
        type: "GET",
        url: "produto.php",
        success: function(data){
            $('.recebeDados').html(data);
        }
    })
}
function atualizarProduto(str){
        var idproduto = str;
        var titulo = $('#titulo-'+str).val();
        var tipo = $('#tipo-'+str+':checked').val();
        var medida = $('#medida-'+str).val();
        var asas = $('#asas-'+str+':checked').val();
        var barbela = $('#barbela-'+str+':checked').val();
        var crista = $('#crista-'+str+':checked').val();
        var rabo = $('#rabo-'+str+':checked').val();
        var retirada = $('#retirada-'+str+':checked').val();
        var idade = $('#idade-'+str).val();
        var preco = $('#preco-'+str).val();
        var descricao = $('#descricao-'+str).val();
        $.ajax({
        type: "POST",
        url: "produto.php?p=editar",
        data: "titulo="+titulo+"&tipo="+tipo+"&medida="+medida+"&asas="+asas+"&barbela="+barbela+"&crista="+crista+"&rabo="+rabo+"&retirada="+retirada+"&idade="+idade+"&preco="+preco+"&descricao="+descricao+"&idproduto="+idproduto,
        success: function(data){
            verDados();
        }
    });
    
}

function atualizarProdutoDois(str){
        var idproduto = str;
        var titulo = $('#tituloDois-'+str).val();
        var tipo = $('#tipoDois-'+str+':checked').val();
        var retirada = $('#retiradaDois-'+str+':checked').val();
        var idade = $('#idadeDois-'+str).val();
        var preco = $('#precoDois-'+str).val();
        var descricao = $('#descricaoDois-'+str).val();
        $.ajax({
        type: "POST",
        url: "produto.php?p=editarpintinho",
        data: "titulo="+titulo+"&tipo="+tipo+"&retirada="+retirada+"&idade="+idade+"&preco="+preco+"&descricao="+descricao+"&idproduto="+idproduto,
        success: function(data){
            verDados();
        }
    });
    
}

function atualizarProdutoTres(str){
        var idproduto = str;
        var titulo = $('#tituloTres-'+str).val();
        var preco = $('#precoTres-'+str).val();
        var retirada = $('#retiradaTres-'+str+':checked').val();
        var tipo = $('#tipoTres-'+str+':checked').val();    
        var descricao = $('#descricaoTres-'+str).val();
        $.ajax({
        type: "POST",
        url: "produto.php?p=editarproduto",
        data: "titulo="+titulo+"&preco="+preco+"&retirada="+retirada+"&tipo="+tipo+"&descricao="+descricao+"&idproduto="+idproduto,
        success: function(data){
            verDados();
        }
    });
    
}

function deletarProduto(str){
    var idproduto = str;
    $.ajax({
        type: "GET",
        url: "produto.php?p=deletar",
        data: "idproduto="+idproduto,
        success: function(data){
            verDados();
        }
    })
}



//-----------------------------------------------------
//Funcao: MascaraMoeda
//Sinopse: Mascara de preenchimento de moeda
//Parametro:
//   objTextBox : Objeto (TextBox)
//   SeparadorMilesimo : Caracter separador de milésimos
//   SeparadorDecimal : Caracter separador de decimais
//   e : Evento
//Retorno: Booleano
//Autor: Gabriel Fróes - www.codigofonte.com.br
//-----------------------------------------------------
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}


function abrirModal(id){
$("#idproduto").val(id);
$("#fotoModal").modal();
}


</script>


<script>
window.onload = function(){
    verDados();
}
</script>

<?php include"footer.php";?>
