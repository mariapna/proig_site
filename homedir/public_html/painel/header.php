<?php

ob_start();
session_start();
include "../classes/config.php";
if(!isset($_SESSION['id'])){
    header('location: ../login.php');
}

function sair() {
session_start();
session_destroy();
header('location: ../login.php');
}
if (isset($_GET['sair'])){

    sair();
    
    }

?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pró IG - O Portal do Índio Gigante</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
     
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
     <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

  <link rel="stylesheet" href="css/style.css">

    </head>

    <body>


    <nav class="navbar navbar-default" style="background-color: #6AC0AC;">

  <div class="container">

    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" class="imgResponsive" src="../img/logo_proig.png">
      </a>
    </div><!--fim navbar-header-->
   
    <ul class="nav navbar-nav">
    <li class="corMenu"><a href="#" onclick="alerta()">INFORMAÇÕES</a></li>
    <li class="corMenu"><a href="../classificado.php">COMPRAR</a></li>
    <li class="corMenu"><a href="cadproduto.php">VENDA</a></li>
    <li class="corMenu"><a href="#" onclick="alerta()">EVENTOS</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a class="messages-link dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-envelope"></span>
              <span class="number">4</span>
            </a>
        </li>
        <li>
        <a class="tasks-link dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-th-list"></span> 
              <span class="number">11</span></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Meus Dados <span class="caret"></span></a>
          
          <ul class="dropdown-menu">
            <li><a href="#">Editar Cadastro</a></li>
            <li><a href="#">Inserir Foto</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="?sair">SAIR</a></li>
          </ul>
        </li>
      </ul>

</div><!--fim container nav-->
</nav>
  
<script>
function alerta(){
  alert('Em breve! Estamos preparando o conteúdo!!!');
}
</script>
<div class="container">