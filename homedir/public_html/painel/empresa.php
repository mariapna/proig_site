<?php
include "config.php";
session_start();
if(!isset($_SESSION['username'])){
    header('location: login.php');
}

$page = isset($_GET['p'])?$_GET['p']:'';
if($page=='add'){
    $empresa = $_POST['empresa'];
    $stmt = $db->prepare("INSERT INTO empresa values('',?)");
    $stmt->bindParam(1,$empresa);
    if($stmt->execute()){
        echo "Empresa adicionada com sucesso!";
    }else{
        echo "Falha ao adicionar empresa";
    }
    
}else if($page=='editar'){
    $idEmpresa = $_POST['idEmpresa'];
    $empresa = $_POST['empresa'];
    $stmt = $db->prepare("UPDATE empresa SET empresa=? WHERE idEmpresa=?");
    $stmt->bindParam(1,$empresa);
    $stmt->bindParam(2,$idEmpresa);
    if($stmt->execute()){
        echo "Empresa atualizada com sucesso!";
    }else{
        echo "Falha ao atualizar empresa";
    }
    
}else if($page=='deletar'){
    $idEmpresa = $_GET['idEmpresa'];
    $stmt = $db->prepare("DELETE FROM empresa WHERE idEmpresa=?");
    $stmt->bindParam(1, $idEmpresa);
    if($stmt->execute()){
        echo "Empresa excluida com sucesso!";
    }else{
        echo "Falha ao excluir empresa";
    }
    
}else{
    $stmt = $db->prepare("SELECT * FROM empresa ORDER BY empresa ASC");
    $stmt->execute();
    while($row = $stmt->fetch()){
        ?>
        <tr>
            <td><?php echo $row['empresa']?></td>
            <td>
                <button class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?php echo $row['idEmpresa'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
                
                
<div class="modal fade" id="editModal-<?php echo $row['idEmpresa'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['idEmpresa'] ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editLabel-<?php echo $row['idEmpresa']?>">Atualizar Empresa</h4>
      </div>
      <form>
      <div class="modal-body">
      <input type="hidden" id="<?php echo $row['idEmpresa']?>" value="<?php echo $row['idEmpresa'] ?>">
      <div class="form-group">
                        <label for="empresa">Empresa</label>
                        <textarea class="form-control" id="empresa-<?php echo $row['idEmpresa']?>"><?php echo $row['empresa'] ?></textarea>
                    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
        <button type="submit" onclick="atualizarEmpresa(<?php echo $row['idEmpresa'] ?>)" class="btn btn-primary">Atualizar</button>
      </div>
      </form>
    </div>
  </div>
</div>
                
                
                <button class="btn btn-danger" onclick="deletarEmpresa(<?php echo $row['idEmpresa']?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                
            </td>
        </tr>
        <?php
    }
}
?>
