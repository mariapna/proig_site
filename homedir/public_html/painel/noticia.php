<?php
include "../classes/config.php";
session_start();
if(!isset($_SESSION['id'])){
    header('location: login.php');
}
$page = isset($_GET['p'])?$_GET['p']:'';
if($page=='add'){
    $titulo = $_POST['titulo'];
    $descricao = $_POST['descricao'];
    date_default_timezone_set('America/Sao_Paulo');
    $data = date('Y-m-d H:i:s');
    $fonte = $_POST['fonte'];
    $stmt = $db->prepare("INSERT INTO noticias values('',?,?,?,?)");
    $stmt->bindParam(1,$titulo);
    $stmt->bindParam(2,$descricao);
    $stmt->bindParam(3,$data);
    $stmt->bindParam(4,$fonte);
    if($stmt->execute()){
        echo "Noticia cadastrada com sucesso!";
    }else{
        echo "Falha ao adicionar noticia";
    }
    
}else if($page=='editar'){
    $idNoticia = $_POST['idNoticia'];
    $titulo = $_POST['titulo'];
    $descricao = $_POST['descricao'];
    date_default_timezone_set('America/Sao_Paulo');
    $data = date('Y-m-d H:i:s');
    $fonte = $_POST['fonte'];
    $stmt = $db->prepare("UPDATE noticias SET titulo=?, descricao=?, data=?, fonte=? WHERE idNoticia=?");
    $stmt->bindParam(1,$titulo);
    $stmt->bindParam(2,$descricao);
    $stmt->bindParam(3,$data);
    $stmt->bindParam(4,$fonte);
    $stmt->bindParam(5,$idNoticia);
    if($stmt->execute()){
        echo "Noticia atualizada com sucesso!";
    }else{
        echo "Falha ao atualizar noticia";
    }
    
}else if($page=='deletar'){
    $idNoticia = $_GET['idNoticia'];
    $stmt = $db->prepare("DELETE FROM noticias WHERE idNoticia=?");
    $stmt->bindParam(1, $idNoticia);
    if($stmt->execute()){
        echo "Noticia excluida com sucesso!";
    }else{
        echo "Falha ao excluir noticia";
    }
    
}else{
    $stmt = $db->prepare("SELECT * FROM noticias");
    $stmt->execute();
    
    foreach($stmt as $row){
        ?>
    <tr>

        <td>

            <?php
           $stmt2 = $db->prepare("SELECT p.*, f.nome_foto FROM noticias p INNER JOIN fotosnoticias f ON p.idNoticia = f.idNoticia WHERE p.idNoticia = '".$row['idNoticia']."'");
       $stmt2->execute();
        foreach($stmt2 as $rows){

        ?>
                <img src="../SITE/noticias/<?php echo $rows['nome_foto']; ?>" width="40"/>

                <?php } ?>
        </td>



        <td>
            <?php echo $row['titulo']?>
        </td>
        <td>
            <?php echo $row['data']?>
        </td>
        <td>
            <?php echo $row['fonte']?>
        </td>
        <td>


<button class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?php echo $row['idNoticia'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>


                <div class="modal fade" id="editModal-<?php echo $row['idNoticia'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $row['idNoticia'] ?>">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="editLabel-<?php echo $row['idNoticia']?>">Atualizar Noticia</h4>
                            </div>
                            <form>
                                <div class="modal-body">
                                    <input type="hidden" id="<?php echo $row['idNoticia']?>" value="<?php echo $row['idNoticia'] ?>">
                                       
                                    <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="titulo-<?php echo $row['idNoticia']?>" value="<?php echo $row['titulo'] ?>">
                    </div>

                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" rows="3" id="descricao-<?php echo $row['idNoticia']?>"><?php echo $row['descricao'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="data-<?php echo $row['idNoticia']?>" value="<?php echo $row['data'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="fonte">Fonte</label>
                        <input type="text" class="form-control" id="fonte-<?php echo $row['idNoticia']?>" value="<?php echo $row['fonte'] ?>">
                    </div>                     
                                       

                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" onclick="atualizarNoticia(<?php echo $row['idNoticia'] ?>)" class="btn btn-primary">Atualizar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <button class="btn btn-danger" onclick="deletarNoticia(<?php echo $row['idNoticia']?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>

            <!--Formulario de Upload de Fotos-->
            <!-- Modal -->

            <?php
    if(isset($_FILES['fotos'])){
        foreach ($_FILES['fotos']['name'] as $file =>$nome_foto){
            $filename = date('Ymd-His', time()).mt_rand().'-'.$nome_foto;
            try{
                if(move_uploaded_file($_FILES['fotos']['tmp_name'][$file], '../SITE/noticias/'.$filename)){
                    $stmt = $db->prepare("insert into fotosnoticias (nome_foto, idNoticia) values(:nome_foto, :idNoticia)");
                    $idNoticia = (int)$_POST['idNoticia'];
					$stmt->bindParam(':nome_foto', $filename, PDO::PARAM_STR);   
					$stmt->bindParam(':idNoticia', $idNoticia, PDO::PARAM_INT);   					
                    $stmt->execute();
                    echo '<script>location.href ="cadNoticia.php"; </script>';
                }
            }catch (PDOException $e) {
                echo "DataBase Error: The user could not be added.<br>".$e->getMessage();
                exit;
            } catch (Exception $e) {
                echo "General Error: The user could not be added.<br>".$e->getMessage();
                exit;
        }
    }
    }
?>
               

                <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="fotolLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="fotoLabel">Enviar Fotos</h4>
                            </div>
                            <form method="post" action='noticia.php' enctype="multipart/form-data">
                                <div class="modal-body">
                                   
                                   
<div class="form-group">
                        <label for="idNoticia">Titulo</label>
                    <select class="form-control" name="idNoticia" id="idNoticia">
                       <?php                          
                    $stmt = $db->prepare("SELECT * FROM noticias ORDER BY titulo ASC");
                        $stmt->execute();
                        while($row = $stmt->fetch()){
                            ?>
                            <option value="<?php echo $row['idNoticia']?>">
                                <?php echo $row['titulo']; ?>
                            </option>
                        <?php } ?>
                    </select>
                    </div>  

                                    <div class="form-group">
                                        <input type="file" name="fotos[]" multiple>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" class="btn btn-primary">UPLOAD</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
<button class="btn btn-default pegaId" data-toggle="modal" data-target="#fotoModal" id="<?php echo $row['idNoticia']?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></button>
                <!--Fim Formulario de Upload de Fotos-->

  

                
        </td>
    </tr>

    <?php
    }
}
?>