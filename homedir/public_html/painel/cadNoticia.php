<?php include"header.php"; ?>


<div class="btn-group btn-group-justified" role="group" aria-label="...">
  <div class="btn-group" role="group">
  <button class="btn btn-default" data-toggle="modal" data-target="#addCateg"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  <b>CADASTRAR AVES</b></button>
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-default" data-toggle="modal" data-target="#addCateg"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  <b>CADASTRAR OVOS</b></button>
  </div>
  <div class="btn-group" role="group">
  <button class="btn btn-default" data-toggle="modal" data-target="#addCateg"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>  <b>CADASTRAR PRODUTOS</b></button>
  </div>
</div>
<br>

<?php if(isset($_GET['ok'])): ?>
	<h1>Inserido com sucesso</h1>
<?php endif; ?>


<div class="modal fade" id="addCateg" tabindex="-1" role="dialog" aria-labelledby="addLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLabel">Inserir Noticia</h4>
            </div>
            <form enctype="multipart/form-data">
                <div class="modal-body">


                    <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="titulo" placeholder="digite o titulo">
                    </div>
                    <div class="form-group">
                        <label for="descricao">Noticia</label>
                        <textarea class="form-control" rows="6" id="descricao"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="fonte">Fonte</label>
                        <input type="text" class="form-control" id="fonte" placeholder="digite a fonte">
                    </div>

      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                    <button type="submit" onclick="salvarNoticia()" class="btn btn-primary">SALVAR</button>
                </div>
            </form>
        </div>
    </div>
</div> <!--fim modal 1-->


<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th width="5%">IMG</th>
            <th width="50%">TITULO</th>
            <th width="16%">DATA</th>
            <th width="15%">FONTE</th>
            <th width="14%" align="right">AÇÃO</th>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>
<div class="row">

</div>
<script>
    function salvarNoticia(){
        var titulo = $('#titulo').val();
        var descricao = $('#descricao').val();
        var fonte = $('#fonte').val(); 
        $.ajax({
            type: "POST",
            url: "noticia.php?p=add",
            data: "titulo="+titulo+"&descricao="+descricao+"&fonte="+fonte,
            success: function(msg){
                alert('Noticia cadastrada com sucesso!');
            }

        });
    }
function verDados(){
    $.ajax({
        type: "GET",
        url: "noticia.php",
        success: function(data){
            $('tbody').html(data);
        }
    })
}
function atualizarNoticia(str){
    var idNoticia = str;
    var titulo = $('#titulo-'+str).val();
    var descricao = $('#descricao-'+str).val();
     var data = $('#data-'+str).val();
    var fonte = $('#fonte-'+str).val();    
    $.ajax({
        type: "POST",
        url: "noticia.php?p=editar",
        data: "titulo="+titulo+"&descricao="+descricao+"&fonte="+fonte+"&idNoticia="+idNoticia,
        success: function(data){
            verDados();
        }
    });
    
}
function deletarNoticia(str){
    var idNoticia = str;
    $.ajax({
        type: "GET",
        url: "noticia.php?p=deletar",
        data: "idNoticia="+idNoticia,
        success: function(data){
            verDados();
        }
    })
}
</script>


<script>
window.onload = function(){
    verDados();
}
</script>

<?php include"footer.php";?>
