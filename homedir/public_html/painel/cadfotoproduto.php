<?php
include "../classes/config.php";
    if(isset($_FILES['fotos'])){
        foreach ($_FILES['fotos']['name'] as $file =>$nomefoto){
            $filename = date('Ymd-His', time()).mt_rand().'-'.$nomefoto;
            try{
                if(move_uploaded_file($_FILES['fotos']['tmp_name'][$file], '../img/uploads/'.$filename)){
                    $stmt = $db->prepare("insert into fotosproduto (nomefoto, idproduto) values(:nomefoto, :idproduto)");
                    $idproduto = (int)$_POST['idproduto'];
					$stmt->bindParam(':nomefoto', $filename, PDO::PARAM_STR);   
					$stmt->bindParam(':idproduto', $idproduto, PDO::PARAM_INT);   					
                    $stmt->execute();
                    echo '<script>location.href ="cadproduto.php"; </script>';
                }
            }catch (PDOException $e) {
                echo "DataBase Error: The user could not be added.<br>".$e->getMessage();
                exit;
            } catch (Exception $e) {
                echo "General Error: The user could not be added.<br>".$e->getMessage();
                exit;
        }
    }
    }
?>