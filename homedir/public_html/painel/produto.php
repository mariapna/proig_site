<?php
include "../classes/config.php";
session_start();
if(!isset($_SESSION['id'])){
    header('location: login.php');
}
$page = isset($_GET['p']) ? $_GET['p']:'';
if($page=='add'){
    
    $titulo = $_POST['titulo'];
    $medida = $_POST['medida'];
    $tipo = $_POST['tipo'];
    $crista = $_POST['crista'];
    $idade = $_POST['idade'];
    $preco = $_POST['preco'];
    $asas = $_POST['asas'];
    $barbela = $_POST['barbela'];
    $rabo = $_POST['rabo'];
    $retirada = $_POST['retirada'];
    $descricao = $_POST['descricao'];
    $id = $_POST['id'];
    
    $stmt = $db->prepare("INSERT INTO produto (titulo, medida, tipo, crista, idade, preco, asas, barbela, rabo, retirada, descricao, id) values (:titulo, :medida, :tipo, :crista, :idade, :preco, :asas, :barbela, :rabo, :retirada, :descricao, :id)");

    $stmt->bindParam(':titulo', $titulo, PDO::PARAM_STR);
    $stmt->bindParam(':medida', $medida, PDO::PARAM_STR);
    $stmt->bindParam(':tipo', $tipo, PDO::PARAM_STR);
    $stmt->bindParam(':crista', $crista, PDO::PARAM_STR);
    $stmt->bindParam(':idade', $idade, PDO::PARAM_STR);
    $stmt->bindParam(':preco', $preco, PDO::PARAM_STR);
    $stmt->bindParam(':asas', $asas, PDO::PARAM_STR);
    $stmt->bindParam(':barbela', $barbela, PDO::PARAM_STR);
    $stmt->bindParam(':rabo', $rabo, PDO::PARAM_STR);
    $stmt->bindParam(':retirada', $retirada, PDO::PARAM_STR);
    $stmt->bindParam(':descricao', $descricao, PDO::PARAM_STR);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    if($stmt->execute()){
        echo "Ave cadastrada com sucesso!";
    }else{
        echo "Falha ao adicionar ave";
    }
    
}else if($page=='addpintinho'){
    $titulo = $_POST['titulo'];
    $tipo = $_POST['tipo'];
    $idade = $_POST['idade'];
    $preco = $_POST['preco'];
    $retirada = $_POST['retirada'];
    $descricao = $_POST['descricao'];
    $id = $_POST['id'];
    
    $stmtDois = $db->prepare("INSERT INTO produto (titulo, tipo, idade, preco, retirada, descricao, id) values (:titulo, :tipo, :idade, :preco, :retirada, :descricao, :id)");

    $stmtDois->bindParam(':titulo', $titulo, PDO::PARAM_STR);
    $stmtDois->bindParam(':tipo', $tipo, PDO::PARAM_STR);
    $stmtDois->bindParam(':idade', $idade, PDO::PARAM_STR);
    $stmtDois->bindParam(':preco', $preco, PDO::PARAM_STR);
    $stmtDois->bindParam(':retirada', $retirada, PDO::PARAM_STR);
    $stmtDois->bindParam(':descricao', $descricao, PDO::PARAM_STR);
    $stmtDois->bindParam(':id', $id, PDO::PARAM_STR);
    if($stmtDois->execute()){
        echo "Pintinho cadastrada com sucesso!";
    }else{
        echo "Falha ao adicionar pintinho";
    }
}else if($page=='addproduto'){
    $titulo = $_POST['titulo'];
    $tipo = $_POST['tipo'];
    $preco = $_POST['preco'];
    $retirada = $_POST['retirada'];
    $descricao = $_POST['descricao'];
    $id = $_POST['id'];
    $qtd = $_POST['qtd'];
    $und = $_POST['und'];
    
    $stmtTres = $db->prepare("INSERT INTO produto (titulo, tipo, preco, retirada, descricao, id, qtd, und) values (:titulo, :tipo, :preco, :retirada, :descricao, :id, :qtd, :und)");

    $stmtTres->bindParam(':titulo', $titulo, PDO::PARAM_STR);
    $stmtTres->bindParam(':tipo', $tipo, PDO::PARAM_STR);
    $stmtTres->bindParam(':preco', $preco, PDO::PARAM_STR);
    $stmtTres->bindParam(':retirada', $retirada, PDO::PARAM_STR);
    $stmtTres->bindParam(':descricao', $descricao, PDO::PARAM_STR);
    $stmtTres->bindParam(':id', $id, PDO::PARAM_STR);
    $stmtTres->bindParam(':qtd', $qtd, PDO::PARAM_STR);
    $stmtTres->bindParam(':und', $und, PDO::PARAM_STR);
    if($stmtTres->execute()){
        echo "Produto cadastrada com sucesso!";
    }else{
        echo "Falha ao adicionar produto";
    }
}else if($page=='editar'){

    $idproduto = $_POST['idproduto'];
    $titulo = $_POST['titulo'];
    $tipo = $_POST['tipo'];
    $medida = $_POST['medida'];
    $asas = $_POST['asas'];
    $barbela = $_POST['barbela'];
    $crista = $_POST['crista'];
    $rabo = $_POST['rabo'];
    $retirada = $_POST['retirada'];
    $idade = $_POST['idade'];
    $preco = $_POST['preco'];
    $descricao = $_POST['descricao'];
    $stmt = $db->prepare("UPDATE produto SET titulo=?, tipo=?, medida=?, asas=?, barbela=?, crista=?, rabo=?, retirada=?, idade=?, preco=?, descricao=? WHERE idproduto=?");
    $stmt->bindParam(1,$titulo);
    $stmt->bindParam(2,$tipo);
    $stmt->bindParam(3,$medida);
    $stmt->bindParam(4,$asas);
    $stmt->bindParam(5,$barbela);
    $stmt->bindParam(6,$crista);
    $stmt->bindParam(7,$rabo);
    $stmt->bindParam(8,$retirada);
    $stmt->bindParam(9,$idade);
    $stmt->bindParam(10,$preco);
    $stmt->bindParam(11,$descricao);
    $stmt->bindParam(12,$idproduto);
    if($titulo == ""){
echo"preencha o campo titulo";
    }
    
    else if($stmt->execute()){
        echo "Ave atualizada com sucesso!";
    }else{
        echo "Falha ao atualizar Ave";
    }
    
}else if($page=='editarproduto'){

    $idproduto = $_POST['idproduto'];
    $titulo = $_POST['titulo'];
    $preco = $_POST['preco'];
    $retirada = $_POST['retirada'];
    $tipo = $_POST['tipo'];
    $descricao = $_POST['descricao'];
    $stmt = $db->prepare("UPDATE produto SET titulo=?, preco=?, retirada=?,  tipo=?, descricao=? WHERE idproduto=?");
    $stmt->bindParam(1,$titulo);
    $stmt->bindParam(2,$preco);
    $stmt->bindParam(3,$retirada);
    $stmt->bindParam(4,$tipo);
    $stmt->bindParam(5,$descricao);
    $stmt->bindParam(6,$idproduto);
    if($titulo == ""){
echo"preencha o campo titulo";
    }
    
    else if($stmt->execute()){
        echo "Produto atualizada com sucesso!";
    }else{
        echo "Falha ao atualizar Produto";
    }
    
}else if($page=='editarpintinho'){

    $idproduto = $_POST['idproduto'];
    $titulo = $_POST['titulo'];
    $tipo = $_POST['tipo'];
    $retirada = $_POST['retirada'];
    $idade = $_POST['idade'];
    $preco = $_POST['preco'];
    $descricao = $_POST['descricao'];
    $stmt = $db->prepare("UPDATE produto SET titulo=?, tipo=?, retirada=?, idade=?, preco=?, descricao=? WHERE idproduto=?");
    $stmt->bindParam(1,$titulo);
    $stmt->bindParam(2,$tipo);
    $stmt->bindParam(3,$retirada);
    $stmt->bindParam(4,$idade);
    $stmt->bindParam(5,$preco);
    $stmt->bindParam(6,$descricao);
    $stmt->bindParam(7,$idproduto);
    if($titulo == ""){
echo"preencha o campo titulo";
    }
    
    else if($stmt->execute()){
        echo "Pintinho atualizada com sucesso!";
    }else{
        echo "Falha ao atualizar Pintinho";
    }
    
}else if($page=='deletar'){
    $idproduto = $_GET['idproduto'];
    $stmt = $db->prepare("DELETE FROM produto WHERE idproduto=?");
    $stmt->bindParam(1, $idproduto);
    if($stmt->execute()){
        echo "Ave excluida com sucesso!";
    }else{
        echo "Falha ao excluir Ave";
    }
    
}else{
    $stmtAve = $db->prepare("SELECT * FROM produto WHERE id = '".$_SESSION['id']."' ORDER BY idproduto ASC");
    $stmtAve->execute();
    foreach($stmtAve as $rowAve){
        ?>
<div class="container">
<div class="panel panel-success">
    <div class="panel-heading">
        <b><?php echo strtoupper($rowAve['titulo']); ?></b>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <?php
           $stmtFotoProduto = $db->prepare("SELECT p.*, f.nomefoto FROM produto p INNER JOIN fotosproduto f ON p.idproduto = f.idproduto WHERE p.idproduto = '".$rowAve['idproduto']."'");
       $stmtFotoProduto->execute();
       foreach($stmtFotoProduto as $rowf){

        ?>
                <img src="../img/uploads/<?php echo $rowf['nomefoto']; ?>" width="40" />

                <?php  } ?>
            </div>

            <div class="col-md-10">
<!--inicio table-->
<?php
if($rowAve['tipo'] == "Reprodutor" OR $rowAve['tipo'] == "Matriz" OR $rowAve['tipo'] == "Frango" OR $rowAve['tipo'] == "Franga"){?>
<div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Medida</th>
      <th scope="col">Idade</th>
      <th scope="col" colspan="2">Preço</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowAve['tipo']; ?></td>
      <td><?php echo $rowAve['medida']; ?> cm</td>
      <td><?php echo $rowAve['idade']; ?> meses</td>
      <td colspan="2">R$ <?php echo $rowAve['preco']; ?></td>
    </tr>
    <tr>
      <th scope="col">Rabo</th>
      <th scope="col">Asas</th>
      <th scope="col">Barbela</th>
      <th scope="col">Crista</th>
      <th scope="col" colspan="2">Retirada</th>
    </tr>
    <tr>
      <td><?php echo $rowAve['rabo']; ?></td>
      <td><?php echo $rowAve['asas']; ?></td>
      <td><?php echo $rowAve['barbela']; ?></td>
      <td><?php echo $rowAve['crista']; ?></td>
      <td colspan="2"><?php echo $rowAve['retirada']; ?></td>
    </tr>
    <tr>
    <th scope="col" colspan="5">Descrição</th>
    </tr>
    <tr>
      <td colspan="5"><?php echo $rowAve['descricao']; ?></td>
    </tr>
  </tbody>
</table>

<?php }else if($rowAve['tipo'] == "Pintinho"){ ?>
    <div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Idade</th>
      <th scope="col">Preço</th>
      <th scope="col">Retirada</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowAve['tipo']; ?></td>
      <td><?php echo $rowAve['idade']; ?></td>
      <td><?php echo $rowAve['preco']; ?></td>
      <td><?php echo $rowAve['retirada']; ?></td>
    </tr>
    <tr>
    <th scope="col" colspan="4">Descrição</th>
    </tr>
    <tr>
      <td colspan="4"><?php echo $rowAve['descricao']; ?></td>
    </tr>
  </tbody>
</table>
<!--fim table-->

<?php }else if($rowAve['tipo'] == "Produto"){ ?>
    <div class="table-responsive">
  <table class="table">
    <tr>
    
    </tr>
  </table>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col">Preço</th>
      <th scope="col">Retirada</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $rowAve['tipo']; ?></td>
      <td><?php echo $rowAve['preco']; ?></td>
      <td><?php echo $rowAve['retirada']; ?></td>
    </tr>
    <tr>
    <th scope="col" colspan="4">Descrição</th>
    </tr>
    <tr>
      <td colspan="4"><?php echo $rowAve['descricao']; ?></td>
    </tr>
  </tbody>
</table>
<!--fim table-->
<?php } ?>
<?php
             date_default_timezone_set('America/Sao_Paulo');
             $date = $rowAve['data'];
             echo $date;
             ?>
            </div><!--FIm-->


        </div>
        <!--Fim Row 1-->

        <div class="row">
            
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>



            <div class="col-md-2">

                <a class="btn btn-default btn-block" href="../detalheproduto.php?idproduto=<?php echo $rowAve['idproduto']?>" target="_blank"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> VER PUBLICAÇÃO</a>
            </div>
            <!--Fim botão ver publicacao-->




            <div class="col-md-2">
            <?php
if($rowAve['tipo'] == "Reprodutor" OR $rowAve['tipo'] == "Matriz" OR $rowAve['tipo'] == "Frango" OR $rowAve['tipo'] == "Franga" ){ ?>
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#editModal-<?php echo $rowAve['idproduto'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDITAR AVE</button>
            <?php }else if ($rowAve['tipo'] == "Pintinho"){?>
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#editModalDois-<?php echo $rowAve['idproduto'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDITAR PINTINHO</button>

           <?php  }else if ($rowAve['tipo'] == "Produto"){?>
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#editModalTres-<?php echo $rowAve['idproduto'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDITAR PRODUTO</button>
           <?php } ?>


                <div class="modal fade" id="editModal-<?php echo $rowAve['idproduto'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel-<?php echo $rowAve['idproduto'] ?>">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="editLabel-<?php echo $rowAve['idproduto']?>">Atualizar Ave à Venda</h4>
                            </div>
                            <div class="modal-body">
                                <form>
<div class="row">
<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="titulo">Titulo</label>
                                        <input type="text" class="form-control" id="titulo-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['titulo'];?>">
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                                <label for="medida">Medida</label>
                                                <input type="text" class="form-control" id="medida-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['medida']?>">
                                            </div>
                                    </div>
</div><!--fim linha titulo-->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $tipo = $rowAve['tipo']; ?>
                                                <label for="tipo">Tipo</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipo-<?php echo $rowAve['idproduto']?>" value="Reprodutor" <?php echo ($tipo == 'Reprodutor')?'checked':'';?>> Reprodutor
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipo-<?php echo $rowAve['idproduto']?>" value="Matriz" <?php echo ($tipo == 'Matriz')?'checked':'';?>> Matriz
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipo-<?php echo $rowAve['idproduto']?>" value="Frango" <?php echo ($tipo == 'Frango')?'checked':'';?>> Frango
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipo-<?php echo $rowAve['idproduto']?>" value="Franga" <?php echo ($tipo == 'Franga')?'checked':'';?>> Franga
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <?php $crista = $rowAve['crista']; ?>
                                                <label for="crista">Crista</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="crista" id="crista-<?php echo $rowAve['idproduto']?>" value="Bola" <?php echo ($crista == 'Bola')?'checked':'';?>> Bola
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="crista" id="crista-<?php echo $rowAve['idproduto']?>" value="Ervilha" <?php echo ($crista == 'Ervilha')?'checked':'';?>> Ervilha
                                                </label>
                                                
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $asas = $rowAve['asas'];?>
                                                <label for="asas">Asas</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="asas" id="asas-<?php echo $rowAve['idproduto']?>" value="Encaixada"<?php echo ($asas == 'Encaixada')?'checked':'';?>> Encaixada
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="asas" id="asas-<?php echo $rowAve['idproduto']?>" value="Desencaixada"<?php echo ($asas == 'Desencaixada')?'checked':'';?>> Desencaixada
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $barbela = $rowAve['barbela'];?>
                                                <label for="barbela">Barbela</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela-<?php echo $rowAve['idproduto']?>" value="Ausente" <?php echo ($barbela == 'Ausente')?'checked':'';?>> Ausente
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela-<?php echo $rowAve['idproduto']?>" value="De Boi" <?php echo ($barbela == 'De Boi')?'checked':'';?>> De Boi
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="barbela" id="barbela-<?php echo $rowAve['idproduto']?>" value="De Boi (com dupla)" <?php echo ($barbela == 'De Boi (com dupla)')?'checked':'';?>> De Boi (com dupla)
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $rabo = $rowAve['rabo'];?>
                                                <label for="rabo">Rabo</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rabo" id="rabo-<?php echo $rowAve['idproduto']?>" value="Baixo" <?php echo ($rabo == 'Baixo')?'checked':'';?>> Baixo
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rabo" id="rabo-<?php echo $rowAve['idproduto']?>" value="Medio Baixo" <?php echo ($rabo == 'Medio Baixo')?'checked':'';?>> Medio Baixo
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rabo" id="rabo-<?php echo $rowAve['idproduto']?>" value="Medio" <?php echo ($rabo == 'Medio')?'checked':'';?>> Medio
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rabo" id="rabo-<?php echo $rowAve['idproduto']?>" value="Medio Alto" <?php echo ($rabo == 'Medio Alto')?'checked':'';?>> Medio Alto
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rabo" id="rabo-<?php echo $rowAve['idproduto']?>" value="Alto" <?php echo ($rabo == 'Alto')?'checked':'';?>> Alto
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $retirada = $rowAve['retirada']; ?>
                                                <label for="retirada">Retirada</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retirada-<?php echo $rowAve['idproduto']?>" value="No Local" <?php echo ($retirada == 'No Local')?'checked':'';?>> No Local
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retirada-<?php echo $rowAve['idproduto']?>" value="à Combinar" <?php echo ($retirada == 'à Combinar')?'checked':'';?>> à Combinar
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idade">Idade da Ave</label>
                                                <input type="text" class="form-control" id="idade-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['idade']?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="preco">Preço da Ave</label>
                                                <input type="text" class="form-control" id="preco-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['preco']?>" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="descricao">Descrição</label>
                                        <textarea class="form-control" rows="6" id="descricao-<?php echo $rowAve['idproduto']?>"><?php echo $rowAve['descricao']?></textarea>
                                    </div>


                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                        <button type="submit" onclick="atualizarProduto(<?php echo $rowAve['idproduto'] ?>)" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            <!--Fim botão atualizar-->


                <div class="modal fade" id="editModalDois-<?php echo $rowAve['idproduto'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabelDois-<?php echo $rowAve['idproduto'] ?>">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="editLabelDois-<?php echo $rowAve['idproduto']?>">Atualizar Ave à Venda</h4>
                            </div>
                            <div class="modal-body">
                                <form>

                                    <div class="form-group">
                                        <label for="titulo">Titulo</label>
                                        <input type="text" class="form-control" id="tituloDois-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['titulo'];?>">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="tipo">Tipo</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipoDois-<?php echo $rowAve['idproduto']?>" value="Pintinho" checked> Pintinho
                                                </label>
                                               
                                            </div>
                                        </div>



                                    </div> <!--FIm row-->




                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <?php $retiradaDois = $rowAve['retirada'];?>
                                                <label for="retirada">Retirada</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retiradaDois-<?php echo $rowAve['idproduto']?>" value="No Local" <?php echo ($retiradaDois == 'No Local')?'checked':'';?>> No Local
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retiradaDois-<?php echo $rowAve['idproduto']?>" value="à Combinar" <?php echo ($retiradaDois == 'à Combinar')?'checked':'';?>> à Combinar
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="idade">Idade da Ave</label>
                                                <input type="text" class="form-control" id="idadeDois-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['idade']?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="preco">Preço da Ave</label>
                                                <input type="text" class="form-control" id="precoDois-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['preco']?>" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="descricao">Descrição</label>
                                        <textarea class="form-control" rows="6" id="descricaoDois-<?php echo $rowAve['idproduto']?>"><?php echo $rowAve['descricao']?></textarea>
                                    </div>


                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                        <button type="submit" onclick="atualizarProdutoDois(<?php echo $rowAve['idproduto'] ?>)" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--Fim botão atualizar-->


            <div class="modal fade" id="editModalTres-<?php echo $rowAve['idproduto'] ?>" tabindex="-1" role="dialog" aria-labelledby="editLabelTres-<?php echo $rowAve['idproduto'] ?>">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="editLabelTres-<?php echo $rowAve['idproduto']?>">Atualizar Produto à Venda</h4>
                            </div>
                            <div class="modal-body">
                                <form>

                                    <div class="form-group">
                                        <label for="titulo">Titulo</label>
                                        <input type="text" class="form-control" id="tituloTres-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['titulo'];?>">
                                    </div>

                                    <div class="row">
                    
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="preco">Preço do Produto</label>
                                                <input type="text" class="form-control" id="precoTres-<?php echo $rowAve['idproduto']?>" value="<?php echo $rowAve['preco']?>" onKeyPress="return(MascaraMoeda(this,'.',',',event))">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                            <?php $retiradaTres = $rowAve['retirada']; ?>
                                                <label for="retirada">Retirada</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retiradaTres-<?php echo $rowAve['idproduto']?>" value="No Local" <?php echo ($retiradaTres == 'No Local')?'checked':'';?>> No Local
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="retirada" id="retiradaTres-<?php echo $rowAve['idproduto']?>" value="à Combinar" <?php echo ($retiradaTres == 'à Combinar')?'checked':'';?>> à Combinar
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tipo">Tipo</label><br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="tipo" id="tipoTres-<?php echo $rowAve['idproduto']?>" value="Produto" checked> Produto
                                                </label>
                                               
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="descricao">Descrição</label>
                                        <textarea class="form-control" rows="6" id="descricaoTres-<?php echo $rowAve['idproduto']?>"><?php echo $rowAve['descricao']?></textarea>
                                    </div>


                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                        <button type="submit" onclick="atualizarProdutoTres(<?php echo $rowAve['idproduto'] ?>)" class="btn btn-primary">Atualizar</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            <!--Fim botão atualizar-->






            <div class="col-md-2">
<!--Formulario de Upload de Fotos-->
            <!-- Modal -->




                <div class="modal fade" id="fotoModal" tabindex="-1" role="dialog" aria-labelledby="fotolLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="fotoLabel">Enviar Fotos</h4>
                            </div>
                            <form method="post" action='cadfotoproduto.php' enctype="multipart/form-data">
                                <div class="modal-body">


                                    <div class="form-group">
                                        <label for="id">Foto</label>
                                        <input type="hidden" name="idproduto" id="idproduto" />
                                    </div>

                                    <div class="form-group">
                                        <input type="file" name="fotos[]" multiple>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                    <button type="submit" class="btn btn-primary">ENVIAR</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>





                <button class="btn btn-default pegaId btn-block" onclick="abrirModal(<?php echo $rowAve['idproduto'];?>);"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> ISERIR FOTO</button>
                <!--Fim Formulario de Upload de Fotos-->

            </div>
            <!--Fim botão inserir foto-->

            <div class="col-md-2">
                <button class="btn btn-danger btn-block" onclick="deletarProduto(<?php echo $rowAve['idproduto']?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> EXCLUIR</button>
            </div>
            <!--Fim botão excluir-->

        </div>
        <!--Fim Row 2-->

    </div>
</div>
</div>
<?php
    }
}
?>
